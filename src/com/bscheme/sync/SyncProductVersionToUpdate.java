package com.bscheme.sync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.util.ConnectionDetector;
import com.bscheme.util.Urls;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.widget.Toast;

public class SyncProductVersionToUpdate extends Service {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserKeyID = "UserIDKey"; 
	public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
	
	Context mContext;
	Transaction mTransaction;
	
	Timer timer;
	TimerTask task;
	// empty constructor 
	public SyncProductVersionToUpdate(){
		super();
	}
	public SyncProductVersionToUpdate(Context context){
		this.mContext = context;
		
		mTransaction = new Transaction(mContext);
	}
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		this.mContext = this;
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		
	}
	


	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		Toast.makeText(mContext, "Sync Product ...", Toast.LENGTH_LONG).show();
//		Bundle bandle = intent.getExtras();
//		int timerValue2 = bandle.getInt("timerValue2");
		task = new TimerTask() {

			@Override
			public void run() {
				sharedpref = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
				editor = sharedpref.edit();
						
				final int hubID = sharedpref.getInt(HubID, 0);
				final int userID = sharedpref.getInt(UserKeyID, 0);
				System.out.println("userid from sync product:  " +userID+ "hub=="+ hubID);
				
				ConnectionDetector connection = new ConnectionDetector(mContext);
				if(connection.isConnectingToInternet()){
					HandlerThread uiThread = new HandlerThread("UIHandler");
					uiThread.run();
					uiThread.getLooper();
					postJSON(hubID,userID);
				}else{
					Toast.makeText(mContext, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
				}
				
			}
		};

		timer = new Timer();
		timer.scheduleAtFixedRate(task, 1000, 2*60*60*1000); // 2hours = 2*60*60*1000
	}
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void postJSON(int hubID, int userID){
//		String uploadURL = "http://testserver.bscheme.com/android-koto/transactionpush.php"; // for test
//		String uploadURL = "http://www.kotosolutions.com/kotodev/android/productversionupdaterequest.php";
		String json;
		Urls urls = new Urls();
		json = buildJson(hubID,userID);
		
		if(json.length() > 2){
			productSyncTask task = new productSyncTask();
			task.execute(urls.productversionupdateUrl,json);
		}else{
			Toast.makeText(mContext, "No product for you", Toast.LENGTH_LONG).show();
		}
				
		
	}
	private String buildJson(int hubID, int userID) {
		// TODO Auto-generated method stub
		ArrayList<HashMap<String, String>> productLists = new ArrayList<HashMap<String,String>>();
		
		JSONObject jObjMain = new JSONObject();
		JSONArray jArray_pID = new JSONArray();
		mTransaction = new Transaction(mContext);
		productLists = mTransaction.getProductVersionInfo();
		
		if (productLists.size() >0) {
			
			try {
				String userQRCode = mTransaction.getUserQRCode(userID);
				jObjMain.put("hubid", hubID);
				jObjMain.put("userid", userID);
				jObjMain.put("userqrcode", userQRCode);
				for (HashMap<String, String> hashMap : productLists) {
					
					JSONObject jObj = new JSONObject();
					
					String p_id = hashMap.get("productid");
					jObj.put("productid", Integer.parseInt(p_id));
					jObj.put("productversion", Integer.parseInt(hashMap.get("productversion")));

					jArray_pID.put(jObj);
				}
				jObjMain.put("productVersionInfo", jArray_pID);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return jObjMain.toString();
	}

	public class productSyncTask extends AsyncTask<String, Void, String>{

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			Toast.makeText(mContext, "Syncing Product Version ...", Toast.LENGTH_LONG).show();
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			PostJSON post = new PostJSON();
			String responce = post.postJSONToServer(params[0], params[1]);
			
			
			return responce;
		}
		
		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			System.out.println("response for product version = " +result);
			
			try {
				JSONObject jsonResult = new JSONObject(result);

				String message = jsonResult.getString("success");

				if (message.equals("1") ){
					System.out.println("succeed");
				
					ArrayList<HashMap<String, String>> arrayList_productInfo = new ArrayList<HashMap<String,String>>();
					
					JSONArray jArray_productUpdate = jsonResult.getJSONArray("updated");
					
					
					for(int i =0; i<jArray_productUpdate.length(); i++){
						System.out.println("updated product # " +jArray_productUpdate);
						
						JSONObject jObj_singleProduct = jArray_productUpdate.getJSONObject(i);
						
							String productid = jObj_singleProduct.getString("productid");
							String productversion = jObj_singleProduct.getString("productversion");
							String isNewProduct = jArray_productUpdate.getJSONObject(i).getString("new");
//							System.out.println("p id = "+productid+" version =" +productversion);
							String productname1 = jArray_productUpdate.getJSONObject(i).getString("productname1");
							String productname2 = jArray_productUpdate.getJSONObject(i).getString("productname2");
							String productimage = jArray_productUpdate.getJSONObject(i).getString("productimage");
							String eproductname1 = jArray_productUpdate.getJSONObject(i).getString("eproductname1");
							String status = jArray_productUpdate.getJSONObject(i).getString("status");
							String eproductname2 = jArray_productUpdate.getJSONObject(i).getString("eproductname2");
							String tp = jArray_productUpdate.getJSONObject(i).getString("tp");
							String mrp = jArray_productUpdate.getJSONObject(i).getString("mrp");
							String createdby = jArray_productUpdate.getJSONObject(i).getString("createdby");
							String specialoffer = jArray_productUpdate.getJSONObject(i).getString("specialoffer");
							String categoryid = jArray_productUpdate.getJSONObject(i).getString("categoryid");
							String unitid = jArray_productUpdate.getJSONObject(i).getString("unitid");
							String updatedby = jArray_productUpdate.getJSONObject(i).getString("updatedby");
							String updated = jArray_productUpdate.getJSONObject(i).getString("updated");
							String created = jArray_productUpdate.getJSONObject(i).getString("created");
							String pp = jArray_productUpdate.getJSONObject(i).getString("pp");
							String manufid = jArray_productUpdate.getJSONObject(i).getString("manufid");
							
							HashMap<String, String> map = new HashMap<String, String>();
							
							map.put("productid", productid);
							map.put("productversion", productversion);
							map.put("new", isNewProduct);
							map.put("productname1", productname1);
							map.put("productname2", productname2);
							map.put("productimage", productimage);
							map.put("eproductname1", eproductname1);
							map.put("eproductname2", eproductname2);
							map.put("status", status);
							map.put("tp", tp);
							map.put("mrp", mrp);
							map.put("createdby", createdby);
							map.put("specialoffer", specialoffer);
							map.put("categoryid", categoryid);
							map.put("unitid", unitid);
							map.put("updatedby", updatedby);
							map.put("updated", updated);
							map.put("created", created);
							map.put("pp", pp);
							map.put("manufid", manufid);

							arrayList_productInfo.add(map);
							
					}
					System.out.println("list p info: "+arrayList_productInfo.toString());
					mTransaction.updateProductInfo(arrayList_productInfo);
					
					Toast.makeText(mContext, " Syncing Done ", Toast.LENGTH_SHORT).show();
				}else if(message.equals("0")){
					System.out.println("status not update");
					Toast.makeText(mContext, " Synced but Not update in server", Toast.LENGTH_SHORT).show();
				}

			} catch (Exception Ex) {
				Ex.printStackTrace();
			}
				
		}
		
	}
}
