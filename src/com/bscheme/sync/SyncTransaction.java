package com.bscheme.sync;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bscheme.jitasalestracker.Message;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.util.ConnectionDetector;
import com.bscheme.util.JSONParser;
import com.bscheme.util.MessageHandler;
import com.bscheme.util.Urls;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

/**
 * 
 * @author Newton
 * 
 */
public class SyncTransaction extends Service{

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserKeyID = "UserIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
    Bundle bundle;
    
	private static final String TAG = "MyService";
	ProgressDialog pDiag;
	Timer timer;
	TimerTask task;
	Urls urls = new Urls();
	Transaction mTransaction;
	PostJSON postJSON;
	Context mContext;
	public int[] transIDListToUpdate ;
	
	public String message = "0";
	public SyncTransaction(){
		super();
	}
	public SyncTransaction(Context c) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		mTransaction = new Transaction(mContext);
	}
	
	public void postJSON(String userid){
		String json;
		json = buildJSON(Integer.parseInt(userid));
		System.out.println("json value ===::::" +json.length());
		Urls urls = new Urls();
		if(json.length() > 2){
			PostToServerAsyncTask call = new PostToServerAsyncTask();
			call.execute(urls.syncTransUrl, json);
		
		}else{
//			Toast.makeText(mContext, "No Transactions", Toast.LENGTH_LONG).show();
//			Message.message(getApplicationContext(), "No Transactions");
			
		}
		
		
		/*if (message != "0") {
			mTransaction = new Transaction(SyncTransaction.this);
			mTransaction.updateTransactionStatus(transIDListToUpdate);
			System.out.println("status update ...");
		}*/
	}


	public String buildJSON(int userID){
		ArrayList<HashMap<String, String>> transactionedItem = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>> transactionList = new ArrayList<HashMap<String,String>>();
		mTransaction = new Transaction(mContext);
		mTransaction.open();
		
		transIDListToUpdate = mTransaction.toUpdateTransactionRow(userID);
		
		transactionList = mTransaction.getTransactionInfo(userID);
		String userQRCode = mTransaction.getUserQRCode(userID);
		JSONObject mainJsonObj = new JSONObject(); // main  json object
		
		new JSONObject();
		
		JSONArray jArray_tID = new JSONArray();
		JSONArray jArray_tDetails = new JSONArray();
		
		if(transactionList.size() > 0){
			try {
				for (HashMap<String, String> hashPmap : transactionList) {

					String t_id = hashPmap.get("transactionid");
					String counterpart_id = hashPmap.get("counterpartid");
					String dateTime = hashPmap.get("datetime");
					String transactiontype = hashPmap.get("transactiontype");
					String totalvalue = hashPmap.get("totalvalue");
					String lat = hashPmap.get("lat");
					String lon = hashPmap.get("lon");
					String locacc = hashPmap.get("locacc");

					transactionedItem = new ArrayList<HashMap<String, String>>();
					transactionedItem = mTransaction
							.getTransactionItemDetails(Integer.parseInt(t_id));
					System.out.println("trans id = " + hashPmap + "items :"
							+ transactionedItem.size());

					JSONObject jObj_tID = new JSONObject();

					jObj_tID.put("transaction_id", Integer.parseInt(t_id)); // single
																			// transaction
																			// id
					jObj_tID.put("user_id", userID);
					jObj_tID.put("counterpartid",
							Integer.parseInt(counterpart_id));
					if (dateTime == null) {
						jObj_tID.put("datetime", " ");
					} else {
						jObj_tID.put("datetime", dateTime);
					}

					jObj_tID.put("transactiontype", transactiontype);
					jObj_tID.put("totalvalue", Float.parseFloat(totalvalue));
					jObj_tID.put("lat", lat);
					jObj_tID.put("lon", lon);
					jObj_tID.put("locacc", locacc);

					jArray_tID.put(jObj_tID);

					new JSONObject();

					JSONObject jObj_singleTransItems;
					for (HashMap<String, String> map : transactionedItem) {

						jObj_singleTransItems = new JSONObject();

						String transId = map.get("transactionid");
						String productid = map.get("productId");
						String productQuantity = map.get("productQuantity");
						String productPrice = map.get("productPrice");
						String subtotalvalue = map.get("subtotalvalue");
						/*
						 * System.out.println("P id = " + productid + "=" +
						 * productQuantity + "=" + productPrice + "=" +
						 * subtotalvalue);
						 */

						jObj_singleTransItems.put("transactionid",
								Integer.parseInt(transId));
						jObj_singleTransItems.put("productid",
								Integer.parseInt(productid));
						jObj_singleTransItems.put("productQuantity",
								Integer.parseInt(productQuantity));
						jObj_singleTransItems.put("productPrice", productPrice);
						jObj_singleTransItems.put("subtotalvalue",
								Float.parseFloat(subtotalvalue));

						jArray_tDetails.put(jObj_singleTransItems);
						// jObj_transID.put("items", jArray_tDetails);
					}
					mainJsonObj.put("TransactionDetails", jArray_tDetails);

				}
				mainJsonObj.put("transactionLists", jArray_tID);
				mainJsonObj.put("userQRCode", userQRCode);
				// mainJsonObj.put("transaction", jObj_transID);
				System.out.println("json = " + mainJsonObj);

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(transactionList.size() <= 0){
			System.out.println("no transaction in list ");
//			Toast.makeText(context, "no transaction in list", Toast.LENGTH_SHORT).show();
//			Message.message(context, "No transaction yet");
		}
		return mainJsonObj.toString();
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		Log.d(TAG, "onCreate");
		mContext = this;
		bundle = new Bundle() ;
		
	}

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		Toast.makeText(mContext, "Sync Transaction ...", Toast.LENGTH_LONG).show();
//		bundle = intent.getExtras();
//		int timerValue = bundle.getInt("timerValue");
		task = new TimerTask() {

			@Override
			public void run() {
				sharedpref = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
				editor = sharedpref.edit();
						
				final int userID = sharedpref.getInt(UserKeyID, 0);
				System.out.println("userid from sync:  " +userID);
				
				ConnectionDetector connection = new ConnectionDetector(mContext);
				if(connection.isConnectingToInternet()){
					HandlerThread uiThread = new HandlerThread("UIHandler");
					uiThread.run();
					uiThread.getLooper();
					postJSON(String.valueOf(userID));
					
				}else{
					Toast.makeText(mContext, "No Internet Connectivity", Toast.LENGTH_SHORT).show();
				}
				
			}
		};

		timer = new Timer();
		timer.scheduleAtFixedRate(task, 1000, 2*60*60*1000); //2 hours = 2*60*60*1000
	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		try {
			
			Log.d(TAG, "Service Stopped...");
			timer.cancel();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
public class PostToServerAsyncTask extends AsyncTask<String, Void, String>{

		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			System.out.println("starting.....");
			Toast.makeText(mContext, "Syncing Transaction...", Toast.LENGTH_SHORT).show();
					
		}

		@Override
		protected String doInBackground(String... params) {
			// TODO Auto-generated method stub
			
			String responce = null;
				PostJSON post = new PostJSON();
				responce = post.postJSONToServer(params[0], params[1]);
				System.out.println("Responce from url = " +responce);
				
				System.out.println("transIDListToUpdate init = " +transIDListToUpdate);
				try {
					JSONObject jsonResult = new JSONObject(responce);

					message = jsonResult.getString("success");

					if (message.equals("1") ){
						System.out.println("succeed");
					
						mTransaction.updateTransactionStatus(transIDListToUpdate);

//						Message.message(SyncTransaction.this, "Status updated");
					}else{
						System.out.println("status not update");
					}

				} catch (Exception Ex) {
					Ex.printStackTrace();
				}
				
			
			return responce;
		}

		@Override
		protected void onPostExecute(String result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(result != null){
				Toast.makeText(mContext, " Syncing Transaction Done ", Toast.LENGTH_SHORT).show();
			}else{
				Toast.makeText(mContext, " No response from server ", Toast.LENGTH_SHORT).show();
			}
			/*pDiag.hide();
			pDiag.setMessage("Synced successful ");
			pDiag.show();
//			System.out.println("Responce from url = " +result);
			pDiag.dismiss();*/

			
			/*try {
				JSONObject jsonResult = new JSONObject(result);

				message = jsonResult.getString("success");

				if (message.equals("1") ){
					System.out.println("succeed");
					
					mTransaction = new Transaction(SyncTransaction.this);
					mTransaction.updateTransactionStatus(transIDListToUpdate);
				}else{
					System.out.println("status not update");
				}

			} catch (Exception Ex) {
				Ex.printStackTrace();
			}*/
		}
	}
	
	
}
