package com.bscheme.jitasalestracker;

import java.text.DecimalFormat;

import com.bscheme.jita.boradcast.NetworkUtil;
import com.bscheme.jitasalestracker.R.id;
import com.bscheme.jitasalestracker.TransactionFinalAdapter.ViewHolder;
import com.bscheme.sync.SyncTransaction;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TransactionFinal extends Activity {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey";
    public static final String UserCounterID = "UserCounterIDKey"; 
    public static final String Time = "TimeKey"; 
    public static final String DistributorID = "DistributorIDKey";
    
    Context mContext;
	String productName;
    String productDesc;
    int productQuantity;
    public String dateTime;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    int productID;
	int transactionID;
    String productImg;
    public int transType;
    float transTotal;
    int transTotalItems;
    String transDate;
    TextView tv_transToatal,tv_transDate,tv_transTypeName;
	
	ListView myList;
	Button cancelTrans, saveTrans;
	
	public int HUB_ID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_transaction_final);
		mContext = this;
		//getting values passed from main activity
		Bundle bndl = getIntent().getExtras();
		transType = bndl.getInt("transType");
		transTotal = Float.valueOf(new DecimalFormat("##.##").format(bndl.getFloat("finalTotal")));
		transTotalItems = bndl.getInt("transTotalItems");
		transDate = bndl.getString("transDate");
//		Message.message(this, ""+transType);
		
		//initializing sharedpreference object
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		int USER_TYPE = sharedpref.getInt(UserTypeID,0);
		final int HUB_ID = sharedpref.getInt(HubID,0);
		final int USER_ID = sharedpref.getInt(UserID,0);
		final int USER_COUNTERPART = sharedpref.getInt(UserCounterID, 0);
		final int DIST_ID = sharedpref.getInt(DistributorID, 0);
		
		//creating button object
		cancelTrans = (Button) findViewById(R.id.btn_changeTransaction);
		saveTrans = (Button) findViewById(R.id.btn_confirmTransaction);
		
		
		if(sharedpref.contains(Time)){
			dateTime = sharedpref.getString(Time, "");
		} 
		
		Bundle bundle = getIntent().getExtras();
		transType = bundle.getInt("transType");
		
		tv_transToatal = (TextView) findViewById(R.id.tv_transTotal);
		tv_transTypeName = (TextView) findViewById(R.id.tv_transTypeName);
		tv_transDate = (TextView) findViewById(R.id.tv_transDate);
		myList = (ListView) findViewById(R.id.finalTransaction);
		
		tv_transToatal.setText("Total: TK. "+ transTotal);
		tv_transDate.setText(transDate);
		/*if (transType == 2) {
			tv_transTypeName.setText("" +transTotalItems+ "Items Bought");
		}else if (transType == 1) {
			tv_transTypeName.setText("" +transTotalItems+ "Items Sold");
		}*/
		myList.setAdapter(new TransactionFinalAdapter(this, dateTime,transType,tv_transTypeName));
//		ViewHolder mViewHolder = new ViewHolder(getCurrentFocus());
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(TransactionFinal.this);
				mPopupMenuItems.show();
			}
		});
		
		cancelTrans.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if(transType == 1){
					Intent salesActivity = new Intent(TransactionFinal.this,SalesActivity.class);
					//Message.message(TransactionFinal.this,""+ transType);
					startActivity(salesActivity);
					
				} else if(transType == 2) {
					Intent buyActivity = new Intent(TransactionFinal.this,BuyActivity.class);
					//Message.message(TransactionFinal.this, ""+transType);
					startActivity(buyActivity);
				}
			}
		});
		
		
		
		saveTrans.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean status;
				SharedPreferences.Editor editor = sharedpref.edit();
				
				Transaction transc = new Transaction(TransactionFinal.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				transc.open();
				if(transType == 1){
					status = transc.saveTransaction(USER_ID, USER_COUNTERPART, transType, dateTime, HUB_ID);
				} else {
					status = transc.saveTransaction(USER_ID, DIST_ID, transType, dateTime, HUB_ID);
				}
				
				if(status){
					editor.putString(Time, "");
					editor.putInt(UserCounterID, 0);
					editor.putInt(DistributorID, 0);
			    	editor.commit();
			    	if(transType == 1){
						Intent salesActivity = new Intent(TransactionFinal.this,SalesActivity.class);
						//Message.message(TransactionFinal.this,""+ transType);
						startActivity(salesActivity);
						Message.message(TransactionFinal.this, "Transaction Complete.");
						boolean conn = NetworkUtil.isInternetOn(TransactionFinal.this);
						if (conn) {
							SyncTransaction sync = new SyncTransaction(mContext);
							sync.postJSON(String.valueOf(USER_ID));
							/*Intent service = new Intent(mContext,SyncTransaction.class);
							sync.startService(service);*/
						}
						
					} else if(transType == 2) {
						Intent buyActivity = new Intent(TransactionFinal.this,BuyActivity.class);
						//Message.message(TransactionFinal.this, ""+transType);
						startActivity(buyActivity);
						Message.message(TransactionFinal.this, "Transaction Complete.");
						/*Intent service = new Intent(TransactionFinal.this, SyncTransaction.class);
						startService(service);*/
						
						boolean conn = NetworkUtil.isInternetOn(TransactionFinal.this);
						if (conn) {
							SyncTransaction sync = new SyncTransaction(mContext);
							sync.postJSON(String.valueOf(USER_ID));
							/*Intent service = new Intent(mContext,SyncTransaction.class);
							sync.startService(service);*/
						}else{
							Toast.makeText(TransactionFinal.this, "No Internet Connection ! Active Connectivity !", Toast.LENGTH_SHORT).show();
						}
					} 
				} else {
					Message.message(TransactionFinal.this, "Transaction Save Failed.");
				}
				transc.close();
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.transaction_final, menu);
		return true;
	}
	
	
	

}
