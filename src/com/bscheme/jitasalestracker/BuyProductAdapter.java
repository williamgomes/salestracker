package com.bscheme.jitasalestracker;


import java.util.ArrayList;

import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Urls;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class BuyProductAdapter extends BaseAdapter {
	
	String productName;
	String productDesc;
	String productImg;
	int productID;
	int productCatID;
	float partnerPrice;
	float tradePrice;
	float retailPrice;
	float productQuantity;
	ArrayList<Product> productList = new ArrayList<Product>();
	Context mContext;
	int productCategoryID;
	int distID;
	int manufID = 0;
	
	BuyProductAdapter(Context context,int productCategoryId, int hubID, int distId) {

		this.mContext = context;
		this.distID = distId;
		this.productCategoryID = productCategoryId;
		Product product = new Product(context, productName, productDesc,
				productImg, productCatID, productID, partnerPrice, tradePrice, retailPrice,productQuantity);
		product.open();

		manufID = product.getManufIdForDistributor(distID);
		if(productCategoryID == 1000){
			System.out.println("star id :" +productCategoryID);
			productList = product.getStarredProducts(context,true, hubID);
		}else{
			productList = product.getProductsInBuy(context,productCategoryID, hubID,manufID);
		}

		product.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return productList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	class ViewHolder {

		TextView productTitle, productDescription,productIDv,productPrice;
		ImageView productImg;

		String pPrice;
		ViewHolder(View view) {
			productTitle = (TextView) view.findViewById(R.id.textTitle);
			productDescription = (TextView) view.findViewById(R.id.textDesc);
			productImg = (ImageView) view.findViewById(R.id.imgProduct);
			productIDv = (TextView) view.findViewById(R.id.textID);
			productPrice = (TextView) view.findViewById(R.id.textPrice);

		}
	}

	@Override
	public View getView(int position, View view, ViewGroup productViewGroup) {
		// TODO Auto-generated method stub
		View productView = view;
		ViewHolder vholder = null;


		if (productView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			productView = inflater.inflate(R.layout.single_product,
					productViewGroup, false);
			vholder = new ViewHolder(productView);
			productView.setTag(vholder);
		} else {
			vholder = (ViewHolder) productView.getTag();
		}

		if(productList.size() >0){
		Product tempProduct = productList.get(position);
		vholder.productTitle.setText(tempProduct.productName);
		vholder.productDescription.setText(tempProduct.productDesc);


		vholder.pPrice = "" + tempProduct.partnerPrice + " | " + tempProduct.tradePrice + " | " + tempProduct.retailPrice;
		String productImageURL = tempProduct.productImg;
		Urls urls = new Urls();
		//System.out.println(""+productImageURL);
		String URL = urls.downloadImageUrl + productImageURL;
		
		vholder.productPrice.setText(vholder.pPrice);
		vholder.productImg.setTag(tempProduct.productImg);
		vholder.productIDv.setTag(tempProduct);
//		DownloadImagesTask dlit = new DownloadImagesTask();
//		dlit.execute(vholder.productImg);
		
		ImageDownloaderTask imgDownload = new ImageDownloaderTask(mContext,vholder.productImg,URL);
		imgDownload.execute();
		}
		return productView;
	}
}
