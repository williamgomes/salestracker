package com.bscheme.jitasalestracker.inventory;

import com.bscheme.jitasalestracker.Message;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.R.layout;
import com.bscheme.jitasalestracker.R.menu;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class InventorySingleProduct extends Activity {

	TextView tv_pName;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_inventory_single_product);
		
		Bundle bundle = getIntent().getExtras();
		int productID = bundle.getInt("productID");
		String pName = bundle.getString("pName");
		
		tv_pName = (TextView) findViewById(R.id.tv_productName);
		tv_pName.setText(pName);
//		Message.message(this, "ProductID: " + productID);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.inventory_product, menu);
		return true;
		
		
	}

}
