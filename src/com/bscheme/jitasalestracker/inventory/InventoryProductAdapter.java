package com.bscheme.jitasalestracker.inventory;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.bscheme.jitasalestracker.Product;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.jitasalestracker.inventory.Inventory.ProductInfo;
import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Urls;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class InventoryProductAdapter extends BaseAdapter {

	public static final String MyPREFERENCES = "MyPrefs";
	public static final String TIME = "TimeKey";
	public static final String UserID = "UserIDKey";
	public static final String HubID = "HubIDKey";
	SharedPreferences sharedpreferences;

	ArrayList<ProductInfo> productList = new ArrayList<ProductInfo>();
	Context mContext;

	ProductInfo tempProduct;
	int hubID;

	Product mProduct;
	InventoryProductAdapter(Context context, int hubId) {

		this.mContext = context;
		this.hubID = hubId;

		mProduct = new Product(mContext);
		mProduct.open();
		productList = mProduct.getAllProductsInventory(mContext, hubID);
		mProduct.close();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return productList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		TextView productTitle,productDesc, productPrice, productCurrentStock, productAvgQ,productWksCover;
		ImageView productImg;
		CheckBox productCheck;
		int productID;
		public String hubname;

		ViewHolder(View view) {
			productTitle = (TextView) view.findViewById(R.id.textProductName);
			productDesc = (TextView) view.findViewById(R.id.tv_ProductDesc);
			productPrice = (TextView) view.findViewById(R.id.textProductPrice);
			productImg = (ImageView) view.findViewById(R.id.imgview_product);
			productCurrentStock = (TextView) view.findViewById(R.id.textStock);
			productAvgQ = (TextView) view.findViewById(R.id.textAvgStock);
			productWksCover = (TextView) view.findViewById(R.id.textWksCover);
			productCheck = (CheckBox) view.findViewById(R.id.chkbox_productChk);

		}
	}

	@Override
	public View getView(int position, View view, ViewGroup viewGroup) {
		// TODO Auto-generated method stub
		View productRowView = view;
		ViewHolder vholder = null;

		if (productRowView == null) { // if it's not recycled, initialize some
										// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			productRowView = inflater.inflate(R.layout.list_item_inventory,
					viewGroup, false);
			vholder = new ViewHolder(productRowView);
			productRowView.setTag(vholder);
		} else {
			vholder = (ViewHolder) productRowView.getTag();
		}

		tempProduct = productList.get(position);

		vholder.productTitle.setText(tempProduct.productName);
		vholder.productDesc.setText(tempProduct.productDesc);
		vholder.productID = tempProduct.productID;
		vholder.hubname = tempProduct.hubName;
		vholder.productCheck.setTag(tempProduct);

		vholder.productCheck.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				CheckBox cb = (CheckBox) v;
				ProductInfo mProductInfo = (ProductInfo) cb.getTag();
				mProduct.open();
				System.out.println("checked is or not: " +cb.isChecked());
				mProductInfo.setChecked(cb.isChecked());
				mProduct.updateStarProduct(hubID,mProductInfo.productID,cb.isChecked());
				
				mProduct.close();
			}

		});
		
		vholder.productCheck.setId(position);
		vholder.productCheck.setFocusable(false);
		vholder.productCheck.setChecked(tempProduct.isSelected());
		vholder.productCheck.setTag(tempProduct);
		
		Urls urls = new Urls();
		String productImageName = tempProduct.productImg;
		// System.out.println(""+productImageURL);
		String productImageURL = urls.downloadImageUrl + productImageName;

		String productPrice = tempProduct.partnerPrice + " / "
				+ tempProduct.tradePrice + " / " + tempProduct.retailPrice;

		sharedpreferences = mContext.getSharedPreferences(MyPREFERENCES,
				mContext.MODE_PRIVATE);
//		final int hubID = sharedpreferences.getInt(HubID, 0);

		Transaction tmpTrans = new Transaction(mContext);
		tmpTrans.open();
		float tmpAvgQuantity = tmpTrans.getAvgQuantitySold(hubID,
				vholder.productID);
		String avgQuantity = new DecimalFormat("##.##").format(tmpAvgQuantity);
		tmpTrans.close();

		// getting weeks cover value
		float tmpWeeksCover = tempProduct.productQuantity / tmpAvgQuantity;
		String weeksCover = new DecimalFormat("##.##").format(tmpWeeksCover);

		vholder.productPrice.setText(productPrice);
		vholder.productCurrentStock.setText(String
				.valueOf(tempProduct.productQuantity));
		vholder.productAvgQ.setText(avgQuantity);

		// checking if average quantity is 0 or not
		if (tmpAvgQuantity == 0) {
			vholder.productWksCover.setText(String.valueOf(0));
		} else {
			vholder.productWksCover.setText(weeksCover);
		}
		
//		notifyDataSetChanged();
		ImageDownloaderTask imgDownload = new ImageDownloaderTask(mContext,vholder.productImg,productImageURL);
		imgDownload.execute();
		
		return productRowView;
	}

}
