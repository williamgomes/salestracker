package com.bscheme.jitasalestracker.inventory;

import java.util.List;

import com.bscheme.jitasalestracker.Hub;
import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.Product;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.inventory.Inventory.ProductInfo;
import com.bscheme.jitasalestracker.inventory.InventoryProductAdapter.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.PopupMenu;
import android.widget.TextView;

public class InventoryActivity extends Activity implements OnItemClickListener {

	public static final String MyPREFERENCES = "MyPrefs";
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey";
	public static final String HubID = "HubIDKey";
	public static final String UserTypeID = "UserTypeIDKey";
	public static final String Time = "TimeKey";

	String productName;
	String productDesc;
	int productQuantity;
	float productPrice;
	public String dateTime;
	Button save;

	ListView myList;
	TextView tv_hubNameInTitle;
	Button btn_popUpMenu;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_inventory);
		Inventory mInventory = new Inventory();
		Hub hubInfo = new Hub();
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		int hubID = sharedpref.getInt(HubID, 0);

		Product mProduct = new Product(this);
		List<Hub> mList = mProduct.getHubInfo(hubID);
		String hubName = mList.get(0).hubName;
		tv_hubNameInTitle = (TextView) findViewById(R.id.tv_hubName);
		tv_hubNameInTitle.setText("Inventory for Hub | " +""+hubName);
		
		btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(InventoryActivity.this);
				mPopupMenuItems.show();
			}
		});
		myList = (ListView) findViewById(R.id.showAllProduct);
		myList.setAdapter(new InventoryProductAdapter(this, hubID));
		myList.setOnItemClickListener(InventoryActivity.this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
		// TODO Auto-generated method stub

		ProductInfo pInfo = (ProductInfo) parent.getItemAtPosition(position);
		int productID = pInfo.productID;
		String hubName = pInfo.hubName;
		String pName = pInfo.productName;
		
		Intent singleProduct = new Intent(InventoryActivity.this,InventorySingleProduct.class);
		Bundle bundle = new Bundle();
		bundle.putInt("productID", productID);
		bundle.putString("pName", pName);
		singleProduct.putExtras(bundle);
		singleProduct.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(singleProduct);
	}

}
