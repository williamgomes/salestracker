package com.bscheme.jitasalestracker.inventory;

public class Inventory {

	public class ProductInfo{
		public String hubName;
		public String productName;
		public String productDesc;
		public String productImg;
		public int productCatID;
		public int productID;
		public float tradePrice;
		public float partnerPrice;
		public float retailPrice;
		public float productQuantity;
		public float stockQuantity;
		public boolean selected;
		public void setChecked(boolean selected) {
			// TODO Auto-generated method stub
			this.selected = selected;
		}

		public boolean isSelected() {
			
			// TODO Auto-generated method stub
			return selected;
		}
	}
}
