package com.bscheme.jitasalestracker;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class Distributors{
	
	private final Context mContext;
    private SQLiteDatabase mDb;
    private DBHelper mDbHelper;
    
    int distributorID;
    int manufactureID;
    String distributorName;
    String distributorEName;
    
    
    public Distributors (Context context, int distID, int manufID, String distName, String distEName){
		this.mContext = context;
		this.distributorID = distID;
		this.manufactureID = manufID;
		this.distributorName = distEName;
		this.distributorEName = distEName;
		mDbHelper = new DBHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
	}
    
	public Distributors (Context context){
		this.mContext = context;
		mDbHelper = new DBHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
	}
	
	public Distributors open() throws SQLException 
    {
        try 
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } 
        catch (SQLException mSQLException) 
        {
            throw mSQLException;
        }
        return this;
    }

    public void close() 
    {
        mDbHelper.close();
    }
    
    
    //getting distributor list from database against hubid
    
    public ArrayList<Distributors> getDistributor(int hubID){
    	
    	String queryGetDistributor = "SELECT * FROM t_hubdist,t_distributor WHERE t_distributor.distid=t_hubdist.distid AND t_hubdist.hubid=?";
		String[] selAgrs = {String.valueOf(hubID)};
		ArrayList<Distributors> distList = new ArrayList<Distributors>();
		
		try {
			Cursor Crsr = mDb.rawQuery(queryGetDistributor, selAgrs);
			Log.i("Distributors","Count: " + Crsr.getCount());
			while (Crsr.moveToNext()){
				Distributors tmpDist = new Distributors(mContext,Crsr.getInt(Crsr.getColumnIndex("distid")), Crsr.getInt(Crsr.getColumnIndex("manufid")), Crsr.getString(Crsr.getColumnIndex("distname")), Crsr.getString(Crsr.getColumnIndex("edistname")));
				distList.add(tmpDist);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return distList;
    	
    }
	
}