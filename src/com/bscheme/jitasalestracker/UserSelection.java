package com.bscheme.jitasalestracker;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bscheme.popupmenu.Help;
import com.jwetherell.quick_response_code.CaptureActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class UserSelection extends AlertDialog implements OnClickListener {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserCounterID = "UserCounterIDKey"; 
	public static final String UserID = "UserIDKey"; 
	public static final String Time = "TimeKey";
	
	Context mContext;
	Button btnAparajita, btnOthers;
	int proID;
	float proPrice;
	float proQuantity;
	int userID;
	String dateTime;
	
	
	protected UserSelection(Context context, int proID, float proPrice, float proQuantity) {
		super(context);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.proID = proID;
		this.proPrice = proPrice;
		this.proQuantity = proQuantity;
		View view = getLayoutInflater().inflate(R.layout.userselection, null);
		setView(view);
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		sharedpref = mContext.getSharedPreferences(MyPREFERENCES, mContext.MODE_PRIVATE);
		
		userID = sharedpref.getInt(UserID, 0);
		
		dateTime = sharedpref.getString(Time, "");
		if(dateTime == ""){
			SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
	    	dateTime = s.format(new Date());
	    	editor.putString(Time, dateTime);
	    	editor.commit();
		}
		
		btnAparajita = (Button) findViewById(R.id.btnAparajita);
		btnAparajita.setOnClickListener(this);
		
		btnOthers = (Button) findViewById(R.id.btnOthers);
		btnOthers.setOnClickListener(this);
	}
	
	
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btnAparajita:

			Intent QRIntent = new Intent(getContext(), CaptureActivity.class); 
			
			QRIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(QRIntent);
			break;
		case R.id.btnOthers:
			
			editor = sharedpref.edit();
			editor.putInt(UserCounterID, 1);
			editor.commit();
			
			Transaction trans = new Transaction(mContext);
			trans.open();
			trans.insertProduct(userID, proID, proPrice, dateTime, proQuantity, 1);
			trans.close();
			
			Intent saleIntent = new Intent(getContext(),SalesActivity.class);
			saleIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(saleIntent);
			
			break;
		}
	}

	
}
