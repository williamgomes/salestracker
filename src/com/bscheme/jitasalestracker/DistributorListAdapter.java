package com.bscheme.jitasalestracker;


import java.util.ArrayList;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

public class DistributorListAdapter extends BaseAdapter {
	
	int distributorID;
    int manufactureID;
    String distributorName;
    String distributorEName;
    ArrayList<Distributors> distList = new ArrayList<Distributors>();
    Context context;
    
    public DistributorListAdapter(Context context, int hubID){
    	this.context = context;
    	
    	//instantiating distributor class
    	Distributors dists = new Distributors(context);
    	
    	dists.open();
    	distList = dists.getDistributor(hubID);
    	dists.close();
    }
    
    
    class ViewHolder {

		TextView txtDistID,txtDistName;

		ViewHolder(View view) {
			txtDistID = (TextView) view.findViewById(R.id.txtDistId);
			txtDistName = (TextView) view.findViewById(R.id.txtDistName);
		}
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return distList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return distList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup productViewGroup) {
		// TODO Auto-generated method stub
		
		View distView = view;
		ViewHolder vholder = null;


		if (distView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			distView = inflater.inflate(R.layout.distributor_listview_item,
					productViewGroup, false);
			vholder = new ViewHolder(distView);
			distView.setTag(vholder);
		} else {
			vholder = (ViewHolder) distView.getTag();
		}

		Distributors tmpDist = distList.get(position);
		Log.i("DistributorAdapter","Ename: " + tmpDist.distributorEName);
		
		vholder.txtDistName.setText(tmpDist.distributorEName);
		vholder.txtDistID.setTag(tmpDist);
		
		return distView;
	}
	
	
}
