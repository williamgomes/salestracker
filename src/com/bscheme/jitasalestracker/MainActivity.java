package com.bscheme.jitasalestracker;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
    public static final String Time = "TimeKey"; 
	
	EditText username, password;
	Button login;
	public String dateTime;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_main);
		
		//initializing objects
		username = (EditText) findViewById(R.id.txtUsername);
		password = (EditText) findViewById(R.id.txtPassword);
		login = (Button) findViewById(R.id.btnLogin);
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
	}
	
	
	
	@Override
	protected void onResume() {
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		
		if (sharedpref.contains(UserTypeID)) {
			
			int USER_TYPE = sharedpref.getInt(UserTypeID,0);
			int HUB_ID = sharedpref.getInt(HubID,0);
			int USER_ID = sharedpref.getInt(UserID,0);
			if (USER_TYPE == 2) {
				
				/* service person =2 */
				Intent sales = new Intent(MainActivity.this,SalesActivity.class);
				startActivity(sales);
				
				
				dateTime = sharedpref.getString(Time, "");
				if(dateTime == ""){
					SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
			    	dateTime = s.format(new Date());
			    	editor.putString(Time, dateTime);
			    	editor.commit();
				}
				
			} else if(USER_TYPE == 3){
				
				/* hub manager =3*/
				Intent buy = new Intent(MainActivity.this,BuyActivity.class);
				startActivity(buy);
				
				
				dateTime = sharedpref.getString(Time, "");
				Log.i("MainActivity","Datetime: " + dateTime);
				if(dateTime == ""){
					SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
			    	dateTime = s.format(new Date());
			    	Log.i("MainActivity","Datetime: " + dateTime);
			    	editor.putString(Time, dateTime);
			    	editor.commit();
				}
				
			}
		}
		super.onResume();
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	
	
	public void userLogin(View view){
		
		
		editor = sharedpref.edit();
		
		String uname = username.getText().toString();
		String pass = password.getText().toString();
		
		LoginAdapter mainLoginHelper = new LoginAdapter(this);        
		mainLoginHelper.createDatabase();      
		mainLoginHelper.open();

		int[] userInfo = mainLoginHelper.loginProcess(uname, pass);
		
		int hubID = userInfo[0];
		int userTypeID = userInfo[1];
		int userID = userInfo[2];
		
		//checking user type and redirecting to designated activity view
		if(userTypeID == 0){
			/* aparajita ==0*/
			Message.message(this, "Wrong Username or Password");
		} else if(userTypeID == 2){
			
			/* saving user data inside preference*/
			editor.putInt(UserTypeID,userTypeID);
			editor.putInt(HubID, hubID);
			editor.putInt(UserID, userID);
			
			/* service person ==2 */
			Intent sales = new Intent(MainActivity.this,SalesActivity.class);
			startActivity(sales);
			
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			editor.commit();
			
			
		} else if(userTypeID == 3){
			
			
			/* saving user data inside preference*/
			editor.putInt(UserTypeID,userTypeID);
			editor.putInt(HubID, hubID);
			editor.putInt(UserID, userID);
			
			/* hub manager ==3*/
			Intent buy = new Intent(MainActivity.this,BuyActivity.class);
			startActivity(buy);
			
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			editor.commit();
			
		} else {
			//for now do nothing
		}

		mainLoginHelper.close();
		
	}

}
