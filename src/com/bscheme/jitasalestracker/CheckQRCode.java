package com.bscheme.jitasalestracker;

import java.util.ArrayList;

import com.bscheme.util.UserContent;
import com.jwetherell.quick_response_code.CaptureActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CheckQRCode extends Activity {
	
	public int transactionType;
	public String QRCode;
	
	String fullName;
    String mobileNo;
    int userID;
    
    ArrayList<UserContent> getCustomerInfo = new ArrayList<UserContent>();
    
    ImageView customerImg;
    TextView customerName, customerPhone;
    Button btnConfirm, btnCancel;
    
    
    public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
    public static final String UserCounterID = "UserCounterIDKey"; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_check_qrcode);
		
		customerName = (TextView) findViewById(R.id.txtPersonName);
		customerPhone = (TextView) findViewById(R.id.txtMobileNo);
		btnConfirm = (Button) findViewById(R.id.btnConfirm);
		btnCancel = (Button) findViewById(R.id.btnCancel);
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		
		//getting information from bundle
  		Bundle bndl = getIntent().getExtras();
  		QRCode = bndl.getString("code");
  		
  		Transaction mTransaction = new Transaction(CheckQRCode.this);
  		String userQRCode = mTransaction.findInAllUserQRCode(QRCode);
  		
  		System.out.println("Code: " + QRCode + "Type: " + transactionType);
  		
  		Transaction customerInfo = new Transaction(CheckQRCode.this);
  		
  		getCustomerInfo = customerInfo.getQRContent(QRCode);
  		if(getCustomerInfo.size() > 0){

			customerName.setText((CharSequence) getCustomerInfo.get(0).fullName);
			customerPhone.setText(getCustomerInfo.get(0).mobileNo);
			userID = getCustomerInfo.get(0).userID;

			System.out.println("CheckQR: " +"user: "+ getCustomerInfo.get(0).mobileNo);
			
			btnConfirm.setText("Ok, sale to this person!!");

  		}else{
  			System.out.println("NO values from database. ");
  		}
			btnConfirm.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					System.out.println("user id ======== ::::::::::" + userID);
					editor.putInt(UserCounterID, userID);
					editor.commit();

					Intent salesactivity = new Intent(CheckQRCode.this,
					SalesActivity.class);
					startActivity(salesactivity);
					
				}
			});

			btnCancel.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					// TODO Auto-generated method stub
					Intent checkQR = new Intent(CheckQRCode.this,
							CaptureActivity.class);
					Bundle bndl = new Bundle();
					bndl.putInt("type", transactionType); // 2 for purchase

					//setting bundle inside the activity
					checkQR.putExtras(bndl);
					startActivity(checkQR);
				}
			});
  		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.check_qrcode, menu);
		return true;
	}

}
