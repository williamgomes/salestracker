package com.bscheme.jitasalestracker.transactions;

import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.informations.SalesByAdapter;
import com.bscheme.jitasalestracker.informations.SalesByAdapter.ViewHolder;
import com.bscheme.jitasalestracker.inventory.InventoryActivity;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class TransactionsActivity extends Activity implements
		OnItemClickListener {

	public static final String MyPREFERENCES = "MyPrefs";
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey";
	public static final String HubID = "HubIDKey";
	int hubID;
	int userID;
	/* grid for show sales person	 */
	GridView salesBy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.activity_transactions);

		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		hubID = sharedpref.getInt(HubID, 0);
		userID = sharedpref.getInt(UserID, 0);

		salesBy = (GridView) findViewById(R.id.gv_transactionInfo);

		SalesByAdapter adaptrSalesBy = new SalesByAdapter(this, hubID);
		salesBy.setAdapter(adaptrSalesBy);
		salesBy.setOnItemClickListener(this);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(TransactionsActivity.this);
				mPopupMenuItems.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.transactions, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		// TODO Auto-generated method stub

		switch (arg0.getId()) {
		case R.id.gv_transactionInfo:
			ViewHolder vHolder = (ViewHolder) view.getTag();
			String personName = (String) vHolder.personName.getText();
			int personId = vHolder.personId;

			// transaction info intent

			Intent transactionsDetailInetent = new Intent(this,TransactionsDetailActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("personName", personName);
			bundle.putInt("personId", personId);
			transactionsDetailInetent.putExtras(bundle);
			startActivity(transactionsDetailInetent);
			break;
		default:
			break;
		}
	}

}
