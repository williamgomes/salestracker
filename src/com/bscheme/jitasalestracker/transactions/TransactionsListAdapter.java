package com.bscheme.jitasalestracker.transactions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.util.Sales.TransactionListItemDetail;
import com.bscheme.util.Sales.TransactionsDetail;


public class TransactionsListAdapter extends BaseAdapter {

	Context mContext;

	List<TransactionsDetail> transactionLists = new ArrayList<TransactionsDetail>();

	public TransactionsListAdapter(Context c, int userID, String currentDate) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
		transactionLists = mTransaction.getTransactionId(userID,currentDate);
		mTransaction.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return transactionLists.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return transactionLists.get(position);
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public class ViewHolder{

		TextView tv_transactionID, tv_transactionTypeDescrp, tv_transactionDate, tv_transUser, tv_totalAmount;

		
		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub

			tv_transactionID = (TextView) view.findViewById(R.id.tv_transactionId);
			tv_transactionTypeDescrp = (TextView) view.findViewById(R.id.tv_transactionTypeDescrp);
			tv_transactionDate = (TextView) view.findViewById(R.id.tv_transactionDate);
			tv_totalAmount = (TextView) view.findViewById(R.id.tv_transactionTotalAmount);
			tv_transUser = (TextView) view.findViewById(R.id.tv_transactionUser);
		}
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View salesByRowView = convertView;
		ViewHolder viewHolder = null;
		
		if(salesByRowView == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			salesByRowView = inflater.inflate(R.layout.list_item_transactions_detail, parent,false);
			viewHolder = new ViewHolder(salesByRowView);
			salesByRowView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) salesByRowView.getTag();
		}

		// Comparator for Descending Order
		Comparator<TransactionsDetail> QuantityDescComparator = new Comparator<TransactionsDetail>() {

			public int compare(TransactionsDetail app1,
					TransactionsDetail app2) {

				return ((Integer) (app2.transactionId))
						.compareTo((Integer) app1.transactionId);
			}
		};

		Collections.sort(transactionLists, QuantityDescComparator);
	
		TransactionsDetail mTransactionId = transactionLists.get(position);
		// set values for show transaction for single row
		viewHolder.tv_transactionID.setText(""+mTransactionId.transactionId);
		viewHolder.tv_transactionTypeDescrp.setText(""+mTransactionId.transactionTypeDescrp);
		viewHolder.tv_transactionDate.setText(""+mTransactionId.transactionDate);
		viewHolder.tv_totalAmount.setText(""+mTransactionId.transactionedValue);
		viewHolder.tv_transUser.setText(""+mTransactionId.transactionedToUser);

		
		return salesByRowView;
	}
}
