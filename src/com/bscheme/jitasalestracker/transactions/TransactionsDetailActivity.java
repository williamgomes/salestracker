package com.bscheme.jitasalestracker.transactions;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.bscheme.jitasalestracker.EditTransaction;
import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.transactions.TransactionsListAdapter.ViewHolder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

public class TransactionsDetailActivity extends Activity implements OnItemClickListener {

	public static final String MyPREFERENCES = "MyPrefs";
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey";
	public static final String HubID = "HubIDKey";
	int hubID;
	int userID;
	/* grid view for lists of transaction numbers for a specific sales person*/
	GridView gv_transactions;

	ListView lv_transactions;
	TextView tv_userName_transaction,tv_transactionId, tv_totalAmount;


	ListView lv_transactionListItems;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// Remove title bar
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		// Remove notification bar
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.transactionsinfo_salesperson);
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		hubID = sharedpref.getInt(HubID, 0);
		userID = sharedpref.getInt(UserID, 0);

		tv_totalAmount = (TextView) findViewById(R.id.tv_totalAmountFosingleTrans);
		tv_userName_transaction = (TextView) findViewById(R.id.tv_aparajitaName12);

		tv_transactionId = (TextView) findViewById(R.id.tv_transactionsinfo_id);
//		gv_transactions = (GridView) findViewById(R.id.gv_transactions);
		lv_transactions = (ListView) findViewById(R.id.lv_transactions);
		lv_transactionListItems = (ListView) findViewById(R.id.lv_transactionsDetails);

		Bundle mBundle = getIntent().getExtras();
		tv_userName_transaction.setText("Transaction List of "+""+mBundle.getString("personName"));
		
		SimpleDateFormat postFormater = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		String currentDate =  postFormater.format(new Date());
		
		TransactionsListAdapter adaptrSalesBy = new TransactionsListAdapter(TransactionsDetailActivity.this,userID,currentDate);
		lv_transactions.setAdapter(adaptrSalesBy);
		lv_transactions.setOnItemClickListener(this);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(TransactionsDetailActivity.this);
				mPopupMenuItems.show();
			}
		});
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.lv_transactions:
			ViewHolder mViewHolder = (ViewHolder) view.getTag();

			int transactionsId = Integer.parseInt((String) mViewHolder.tv_transactionID.getText());
			// transaction details for single trnasaction

//			com.bscheme.jitasalestracker.transactions.TransactionsListItemsAdapter.ViewHolder vh = (com.bscheme.jitasalestracker.transactions.TransactionsListItemsAdapter.ViewHolder) view.getTag();
			tv_totalAmount.setText("Total= " + mViewHolder.tv_totalAmount.getText());
			
			tv_transactionId.setText("Transaction detail for id "+""+transactionsId);
			TransactionsListItemsAdapter mTransactionsListItemsAdapter = new TransactionsListItemsAdapter(TransactionsDetailActivity.this,transactionsId,tv_totalAmount);
			lv_transactionListItems.setAdapter(mTransactionsListItemsAdapter);
			mTransactionsListItemsAdapter.notifyDataSetChanged();
			break;

		default:
			break;
		}
	}

}
