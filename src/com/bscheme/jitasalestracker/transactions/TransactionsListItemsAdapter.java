package com.bscheme.jitasalestracker.transactions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.util.Sales.TransactionListItemDetail;
import com.bscheme.util.Sales.YesterdaySalesInfo;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TransactionsListItemsAdapter extends BaseAdapter {

	Context mContext;
	int transactionsID;
	List<TransactionListItemDetail> mListTransactionItems = new ArrayList<TransactionListItemDetail>();

	TextView tv_TotalAmount;
	public TransactionsListItemsAdapter(Context c, int transactionsId, TextView tv_totalAmount) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		this.transactionsID = transactionsId;
		this.tv_TotalAmount = tv_totalAmount;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
		mListTransactionItems = mTransaction
				.getTransactionListItemDetails(transactionsID);
		mTransaction.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mListTransactionItems.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mListTransactionItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		TextView productDetails,subTotalValue;

		String productName, productDesc;

		float pp;

		int pTotalQuantity =0;

		float tp;

		int productQuantity = 0;

		float mrp;
		float subTotalAmount = 0, totalAmount = 0;
		
		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub

			productDetails = (TextView) view.findViewById(R.id.tv_productDtails);
			subTotalValue = (TextView) view.findViewById(R.id.tv_totalSubAmount);
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View transItemsDtailRowView = convertView;
		ViewHolder viewHolder = null;

		if (viewHolder == null) {
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			transItemsDtailRowView = inflater.inflate(
					R.layout.list_item_serviceperson_info, parent, false);
			viewHolder = new ViewHolder(transItemsDtailRowView);
			transItemsDtailRowView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) transItemsDtailRowView.getTag();
		}

		TransactionListItemDetail mTransactionListItemDetail = mListTransactionItems.get(position);
		
		int pp,tp,mrp,productQuantity = 0,pTotalQuantity =0;
		float subTotalAmount = 0, totalAmount = 0;
		
		viewHolder.productName = mTransactionListItemDetail.productName;
		viewHolder.productDesc = mTransactionListItemDetail.productDesc;

		Comparator<TransactionListItemDetail> QuantityDescComparator = new Comparator<TransactionListItemDetail>() {

			public int compare(TransactionListItemDetail app1, TransactionListItemDetail app2) {

				return ((Float) (app2.productSubTotalValue))
						.compareTo((Float) app1.productSubTotalValue);
			}
		};
		
		Collections.sort(mListTransactionItems, QuantityDescComparator);
		
		viewHolder.mrp = mTransactionListItemDetail.retailPrice;
		viewHolder.pp = mTransactionListItemDetail.partnerPrice;
		viewHolder.tp = mTransactionListItemDetail.tradePrice;
		viewHolder.productQuantity = (int) mTransactionListItemDetail.productQuantity;
		viewHolder.subTotalAmount = mTransactionListItemDetail.productSubTotalValue;
		viewHolder.totalAmount = mTransactionListItemDetail.totalAmount;
		// set all product details for a single transaction
		viewHolder.productDetails.setText(""+viewHolder.productQuantity+" x "+mTransactionListItemDetail.productName+" "+""+mTransactionListItemDetail.productDesc+", "+"TK."+""+viewHolder.tp);
		viewHolder.subTotalValue.setText("TK."+""+String.valueOf(viewHolder.subTotalAmount));

//		tv_TotalAmount.setText("Total= "+String.valueOf(mTransactionListItemDetail.totalAmount));
		
		return transItemsDtailRowView;
	}

}
