package com.bscheme.jitasalestracker;

import android.content.Context;
import android.widget.Toast;




public class Message{
	
	public static void message(Context cntx, String message){
		
		Toast.makeText(cntx, message, Toast.LENGTH_SHORT).show();
		
	}
	
}