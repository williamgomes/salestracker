package com.bscheme.jitasalestracker;

import java.text.DecimalFormat;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.GestureDetector.OnGestureListener;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class EditTransaction extends Activity implements OnClickListener {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
    public static final String Time = "TimeKey"; 
    
    
    Context context;
	String productName;
    String productDesc;
    int productQuantity;
    public String dateTime;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    String productImg;
    public int transType;
    
    EditTransactionAdapter editTransAdapter;
	
	ListView myList;
	Button cancelTrans, saveTrans, btn_addMore,tv_transDone;
	
	public int HUB_ID;
	
	ListView transactionList;
	ImageView btnDelete;
	TextView tv_transTypeName;
	int transTotalItems;

	Transaction mtTransaction;
	float finalTotalTransValue;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_edit_transaction);
		
		//initializing sharedpreference object
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		
		int USER_TYPE = sharedpref.getInt(UserTypeID,0);
		final int HUB_ID = sharedpref.getInt(HubID,0);
		final int USER_ID = sharedpref.getInt(UserID,0);
		
		if(sharedpref.contains(Time)){
			dateTime = sharedpref.getString(Time, "");
		} 
		
		Bundle bundle = getIntent().getExtras();
		transType = bundle.getInt("transType");
		String totalTransValue = new DecimalFormat("##.##").format(bundle.getFloat("totalTransValue"));
		
		btn_addMore = (Button) findViewById(R.id.btn_addMore);
		tv_transDone = (Button) findViewById(R.id.tv_transactionDone);
		tv_transTypeName = (TextView) findViewById(R.id.tv_transactionEditTypeName);
		transactionList = (ListView) findViewById(R.id.listEditInventory);
		
		mtTransaction = new Transaction(EditTransaction.this);
		finalTotalTransValue = mtTransaction.getTotalAmount(dateTime);
		
		if(transType == 2){
			tv_transTypeName.setText("Buy These Items?");
			tv_transDone.setText("Total TK. " +finalTotalTransValue+ " Buy Items");
		}else if(transType == 1){
			tv_transTypeName.setText("Sell These Items?");
			tv_transDone.setText("Total TK. " +finalTotalTransValue+ " Sell Items");
		}
//		tv_transDone.setText("Total TK. " +finalTotalTransValue+ "Buy Items");
		tv_transDone.setOnClickListener(this);
		btn_addMore.setOnClickListener(this);
		transactionList.setItemsCanFocus(true);
		editTransAdapter = new EditTransactionAdapter(EditTransaction.this, dateTime,transType);
		transactionList.setAdapter(editTransAdapter);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(EditTransaction.this);
				mPopupMenuItems.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_transaction, menu);
		return true;
	}

	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.tv_transactionDone:
			Intent finalTransaction = new Intent(EditTransaction.this,TransactionFinal.class);
			
			//initializing bundle to carry values
			Bundle bndl = new Bundle();
			if (transType == 2) {
				bndl.putInt("transType", 2); //2 = purchase
			}else if(transType == 1){
				bndl.putInt("transType", 1); //1 = Sell
			}
			
			bndl.putFloat("finalTotal", finalTotalTransValue);
			bndl.putString("transDate", dateTime);
			bndl.putInt("transTotalItems", transTotalItems);
			//setting bundle inside the activity
			finalTransaction.putExtras(bndl);
			finalTransaction.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(finalTransaction);
			break;

		case R.id.btn_addMore:
			if (transType == 2) {
				Intent intBuy = new Intent(EditTransaction.this,BuyActivity.class);
				intBuy.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intBuy);
			}else if(transType == 1){
				Intent intSell = new Intent(EditTransaction.this,SalesActivity.class);
				intSell.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intSell);
			}
			break;
		default:
			break;
		}
	}
	
}
