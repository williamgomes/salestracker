package com.bscheme.jitasalestracker.informations;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.jitasalestracker.R.id;
import com.bscheme.jitasalestracker.R.layout;
import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Sales.SalesTo;

public class SalesToAdapter extends BaseAdapter{
	Context mContext;

	
	List<SalesTo> salesToLists = new ArrayList<SalesTo>();
	public SalesToAdapter(Context c,int hubId) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
		salesToLists = mTransaction.getSalesToPerson(hubId);
		mTransaction.close();
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return salesToLists.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return salesToLists.get(position);
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	class ViewHolder{
		ImageView personLogo;
		TextView personName;
		public int personId;
		String aparajitaVillage,aparajitaLocation,aparajitaDistance,aparajitaJoined;
		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub
			personLogo = (ImageView) view.findViewById(R.id.iv_personLogo);
			personName = (TextView) view.findViewById(R.id.tv_personName);
		}
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View salesToView = convertView;
		ViewHolder viewHolder = null;
		
		if(salesToView == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			salesToView = inflater.inflate(R.layout.list_item_salesto, parent,false);
			viewHolder = new ViewHolder(salesToView);
			salesToView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) salesToView.getTag();
		}
		
		SalesTo salesTo = salesToLists.get(position);
		
//		viewHolder.personLogo.setImageURI(salesTo.personLogo);
		viewHolder.personName.setText(salesTo.personName);
		viewHolder.personId = salesTo.personId;
		viewHolder.aparajitaLocation = salesTo.aparajitaLocation;
		viewHolder.aparajitaJoined = salesTo.aparajitaJoined;
		String imageUrl = salesTo.personLogo;
		
		ImageDownloaderTask imgDwn = new ImageDownloaderTask(mContext, viewHolder.personLogo, imageUrl);
//		imgDwn.execute();
		
		return salesToView;
	}
}