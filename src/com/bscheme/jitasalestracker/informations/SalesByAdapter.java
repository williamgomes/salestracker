package com.bscheme.jitasalestracker.informations;

import java.util.ArrayList;
import java.util.List;

import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.jitasalestracker.R.id;
import com.bscheme.jitasalestracker.R.layout;
import com.bscheme.jitasalestracker.informations.SalesToAdapter.ViewHolder;
import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Sales.SalesBy;
import com.bscheme.util.Sales.SalesTo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SalesByAdapter extends BaseAdapter{

	Context mContext;
	List<SalesBy> salesByLists = new ArrayList<SalesBy>();
	public SalesByAdapter(Context c, int hubID) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
		salesByLists = mTransaction.getSalesByPerson(hubID);
		mTransaction.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return salesByLists.size();
	}
	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return salesByLists.get(position);
	}
	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public class ViewHolder{
		ImageView personLogo;
		public TextView personName;
		public int personId;
		
		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub
//			personId = (TextView) view.findViewById(R.id.tv_personId);
					
			personLogo = (ImageView) view.findViewById(R.id.iv_personLogo);
			personName = (TextView) view.findViewById(R.id.tv_personName);
		}
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View salesByRowView = convertView;
		ViewHolder viewHolder = null;
		
		if(salesByRowView == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			salesByRowView = inflater.inflate(R.layout.list_item_salesto, parent,false);
			viewHolder = new ViewHolder(salesByRowView);
			salesByRowView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) salesByRowView.getTag();
		}
		
		SalesBy salesBy = salesByLists.get(position);
		
		viewHolder.personName.setText(salesBy.personName);
		viewHolder.personId = salesBy.personId;
		String imageUrl = salesBy.personLogo;
		
		/*
		 * get sales details for individuals 
		 */
		
		ImageDownloaderTask imgDwn = new ImageDownloaderTask(mContext, viewHolder.personLogo, imageUrl);
//		imgDwn.execute();
		
		return salesByRowView;
	}

}
