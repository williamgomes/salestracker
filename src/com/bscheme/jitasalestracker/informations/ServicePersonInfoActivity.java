package com.bscheme.jitasalestracker.informations;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.R.id;
import com.bscheme.jitasalestracker.R.layout;
import com.bscheme.jitasalestracker.R.menu;
import com.bscheme.jitasalestracker.inventory.InventoryActivity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ServicePersonInfoActivity extends Activity {

	Bundle bundle;
	String personName;
	int personId;
	TextView tv_personName ,tv_servicePersonName,tv_servicePersonName_sales;
	ListView yesterdaySalesInfoList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_service_person_info);
		bundle = getIntent().getExtras();
		personName = bundle.getString("personName");
		personId = bundle.getInt("personId");
		
		tv_servicePersonName = (TextView) findViewById(R.id.tv_aparajitaName12);
		tv_servicePersonName.setText("Mr. "+""+personName);
		tv_personName = (TextView) findViewById(R.id.tv_personName);
		tv_personName.setText("Mr. "+"" +personName);
		tv_servicePersonName_sales = (TextView) findViewById(R.id.tv_servicePersonName_sales);
		tv_servicePersonName_sales.setText("Mr. " + "" +personName);
		
		yesterdaySalesInfoList = (ListView) findViewById(R.id.lv_saleByPrevInfo);
		ServicePersonInfoAdapter mServicePersonInfoAdapter = new ServicePersonInfoAdapter(this,personId);
		yesterdaySalesInfoList.setAdapter(mServicePersonInfoAdapter);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(ServicePersonInfoActivity.this);
				mPopupMenuItems.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.service_person_info, menu);
		return true;
	}

}
