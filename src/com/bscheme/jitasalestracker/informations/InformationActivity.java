package com.bscheme.jitasalestracker.informations;

import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.informations.SalesByAdapter.ViewHolder;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

public class InformationActivity extends Activity implements OnItemClickListener {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
	int hubID;
	int userID;
	GridView salesBy, salesTo;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.activity_information);
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		hubID = sharedpref.getInt(HubID, 0);
		userID = sharedpref.getInt(UserID, 0);
		
		salesBy = (GridView) findViewById(R.id.gv_salesBy);
		salesTo = (GridView) findViewById(R.id.gv_salesTo);
		
		SalesByAdapter adaptrSalesBy = new SalesByAdapter(InformationActivity.this,hubID);
		salesBy.setAdapter(adaptrSalesBy);
		salesBy.setOnItemClickListener(this);
		
		SalesToAdapter adaptrSalesTo = new SalesToAdapter(InformationActivity.this,hubID);
		salesTo.setAdapter(adaptrSalesTo);
		salesTo.setOnItemClickListener(this);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(InformationActivity.this);
				mPopupMenuItems.show();
			}
		});
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.information, menu);
		return true;
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {
		case R.id.gv_salesBy:
			ViewHolder vHolder = (ViewHolder) view.getTag();
			String personName = (String) vHolder.personName.getText();
			int personId= vHolder.personId;
			
			Intent servicePersonInfoInetent = new Intent(this,ServicePersonInfoActivity.class);
			Bundle bundle = new Bundle();
			bundle.putString("personName", personName);
			bundle.putInt("personId", personId);
			servicePersonInfoInetent.putExtras(bundle);
			startActivity(servicePersonInfoInetent);
			break;

		case R.id.gv_salesTo:
			com.bscheme.jitasalestracker.informations.SalesToAdapter.ViewHolder viewHolder = (com.bscheme.jitasalestracker.informations.SalesToAdapter.ViewHolder) view.getTag();
			int aparajitaId = viewHolder.personId;
			String aparajitaName = (String) viewHolder.personName.getText();
			String aparajitaVillage,aparajitaLocation,aparajitaDistance,aparajitaJoined;
			aparajitaLocation = viewHolder.aparajitaLocation;
			aparajitaJoined = viewHolder.aparajitaJoined;
			Intent aparajitaInfoInetent = new Intent(this,AparajitaInfoActivity.class);
			bundle = new Bundle();
			bundle.putString("aparajitaName", aparajitaName);
			bundle.putInt("aparajitaId", aparajitaId);
			bundle.putString("aparajitaLocation", aparajitaLocation);
			bundle.putString("aparajitaJoined", aparajitaJoined);
			
			aparajitaInfoInetent.putExtras(bundle);
			
			startActivity(aparajitaInfoInetent);
			break;
		default:
			break;
		}
	}

}
