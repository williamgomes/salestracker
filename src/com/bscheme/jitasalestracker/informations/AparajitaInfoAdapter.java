package com.bscheme.jitasalestracker.informations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.jitasalestracker.informations.ServicePersonInfoAdapter.ViewHolder;
import com.bscheme.util.Sales.AparajitaInfo;
import com.bscheme.util.Sales.YesterdaySalesInfo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AparajitaInfoAdapter extends BaseAdapter {

	Context mContext;
	int aparajitaID;
	List<YesterdaySalesInfo> aparjitaInfoLists = new ArrayList<YesterdaySalesInfo>();

	public AparajitaInfoAdapter(Context c, int aparajitaId) {
		// TODO Auto-generated constructor stub
		this.mContext = c;
		this.aparajitaID = aparajitaId;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
//		aparjitaInfoLists = mTransaction.getAparajitaInfo(aparajitaID);
		mTransaction.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return aparjitaInfoLists.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return aparjitaInfoLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		TextView productDetails, totalValue;

		String productName, productDesc;

		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub

			productDetails = (TextView) view
					.findViewById(R.id.tv_productDtails);
			totalValue = (TextView) view.findViewById(R.id.tv_totalSubAmount);
		}
	}

	@Override
	public View getView(int arg0, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View aparajitaInfoRowView = convertView;
		ViewHolder viewHolder = null;
		
		if(viewHolder == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			aparajitaInfoRowView = inflater.inflate(R.layout.list_item_serviceperson_info, parent,false);
			viewHolder = new ViewHolder(aparajitaInfoRowView);
			aparajitaInfoRowView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) aparajitaInfoRowView.getTag();
		}
		
		 //Comparator for Descending Order
	    Comparator<YesterdaySalesInfo> QuantityDescComparator = new Comparator<YesterdaySalesInfo>() {

	        public int compare(YesterdaySalesInfo app1, YesterdaySalesInfo app2) {

	            return ((Integer)(app2.pTotalQuantity)).compareTo((Integer)app1.pTotalQuantity);
	        }
	    };
		Collections.sort(aparjitaInfoLists, QuantityDescComparator);
		
		int pp,tp,mrp,productQuantity = 0,pTotalQuantity =0;
		float subTotalAmount = 0, totalAmount = 0;
		
		return aparajitaInfoRowView;
	}

}
