package com.bscheme.jitasalestracker.informations;

import com.bscheme.jitasalestracker.PopupMenuItems;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.R.layout;
import com.bscheme.jitasalestracker.R.menu;
import com.bscheme.jitasalestracker.inventory.InventoryActivity;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class AparajitaInfoActivity extends Activity {

	Bundle bundle;
	int aparajitaId;
	String aparajitaName,aparajitaVillage,aparajitaLocation,aparajitaDistance,aparajitaJoined;
	ListView lastSaleToAparajita;
	TextView tv_aparajitaName,tv_aparajitaName1,tv_aparajitaName2,tv_aparajitaName3;
	TextView tv_aparajitaVillage, tv_aparajitaLocation,tv_aparajitaDistance,tv_aparajitaJoined;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_aparajita_info);
		
		bundle = getIntent().getExtras();
		aparajitaId = bundle.getInt("aparajitaId");
		aparajitaName = bundle.getString("aparajitaName");
		aparajitaLocation = bundle.getString("aparajitaLocation");
		aparajitaJoined = bundle.getString("aparajitaJoined");
		
		tv_aparajitaName = (TextView) findViewById(R.id.tv_aparajitaName);
		tv_aparajitaName1 = (TextView) findViewById(R.id.tv_aparajitaName1);
		tv_aparajitaName2 = (TextView) findViewById(R.id.tv_aparajitaName2);
		tv_aparajitaName3 = (TextView) findViewById(R.id.tv_aparajitaName3);
		tv_aparajitaVillage = (TextView) findViewById(R.id.tv_productName);
		tv_aparajitaLocation = (TextView) findViewById(R.id.tv_ProductDesc);
		tv_aparajitaDistance = (TextView) findViewById(R.id.tv_Price);
		tv_aparajitaJoined = (TextView) findViewById(R.id.tv_aparajitaJoined);
		
		tv_aparajitaName.setText(aparajitaName);
		tv_aparajitaName1.setText(aparajitaName);
		tv_aparajitaName2.setText(aparajitaName);
		tv_aparajitaName3.setText(aparajitaName);
		tv_aparajitaLocation.setText("Reg. location: "+""+aparajitaLocation);
		tv_aparajitaJoined.setText("Joined: "+""+aparajitaJoined);
		lastSaleToAparajita = (ListView) findViewById(R.id.lv_lastSaleToAparajita);
		
		ServicePersonInfoAdapter mAparajitaInfoAdapter = new ServicePersonInfoAdapter(this, aparajitaId);
		lastSaleToAparajita.setAdapter(mAparajitaInfoAdapter);
		
		Button btn_popUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popUpMenu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(AparajitaInfoActivity.this);
				mPopupMenuItems.show();
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.aparajita_info, menu);
		return true;
	}

}
