package com.bscheme.jitasalestracker.informations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.bscheme.jitasalestracker.R;
import com.bscheme.jitasalestracker.Transaction;
import com.bscheme.util.Sales.YesterdaySalesInfo;

public class ServicePersonInfoAdapter extends BaseAdapter{

	Context mContext;
	int personID;
	List<YesterdaySalesInfo> yesterdaySalesInfoList = new ArrayList<YesterdaySalesInfo>();
	ServicePersonInfoAdapter(Context c,int personId) {
		// TODO Auto-generated constructor stub
		System.out.println("adapter on service person");
		this.mContext = c;
		this.personID = personId;
		Transaction mTransaction = new Transaction(mContext);
		mTransaction.open();
		yesterdaySalesInfoList = mTransaction.getSalesInfo(personID);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return yesterdaySalesInfoList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return yesterdaySalesInfoList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder{
		
		TextView productDetails,totalValue;
		
		String productName,productDesc;
		
		public ViewHolder(View view) {
			// TODO Auto-generated constructor stub
					
			productDetails = (TextView) view.findViewById(R.id.tv_productDtails);
			totalValue = (TextView) view.findViewById(R.id.tv_totalSubAmount);
		}
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View salesPersonInfoRowView = convertView;
		ViewHolder viewHolder = null;
		
		if(viewHolder == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			salesPersonInfoRowView = inflater.inflate(R.layout.list_item_serviceperson_info, parent,false);
			viewHolder = new ViewHolder(salesPersonInfoRowView);
			salesPersonInfoRowView.setTag(viewHolder);
		}else{
			viewHolder = (ViewHolder) salesPersonInfoRowView.getTag();
		}
		
		 //Comparator for Descending Order
	    Comparator<YesterdaySalesInfo> QuantityDescComparator = new Comparator<YesterdaySalesInfo>() {

	        public int compare(YesterdaySalesInfo app1, YesterdaySalesInfo app2) {

	            return ((Integer)(app2.pTotalQuantity)).compareTo((Integer)app1.pTotalQuantity);
	        }
	    };
		Collections.sort(yesterdaySalesInfoList, QuantityDescComparator);
		
		YesterdaySalesInfo mYesterdaySalesInfo = yesterdaySalesInfoList.get(position);
		
		int pp,tp,mrp,productQuantity = 0,pTotalQuantity =0;
		float subTotalAmount = 0, totalAmount = 0;
		
		viewHolder.productName = mYesterdaySalesInfo.pName;
		viewHolder.productDesc = mYesterdaySalesInfo.pDesc;
		pp = mYesterdaySalesInfo.pp;
		tp = mYesterdaySalesInfo.tp;
		mrp = mYesterdaySalesInfo.mrp;
		
		pTotalQuantity = mYesterdaySalesInfo.pTotalQuantity;
		
		totalAmount = mYesterdaySalesInfo.pTotalPrice;
		System.out.println("total quantity:"+pTotalQuantity+ "total amount :" +totalAmount);
		
		
		//set textview
		viewHolder.productDetails.setText(""+pTotalQuantity+" x "+" "+viewHolder.productName+" "+viewHolder.productDesc+", "+"TK. "+"" +pp+"/"+""+tp+"/"+""+mrp);
		viewHolder.totalValue.setText("TK. "+""+totalAmount);
		
		return salesPersonInfoRowView;
	}

}
