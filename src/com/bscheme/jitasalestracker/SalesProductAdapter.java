package com.bscheme.jitasalestracker;


import java.util.ArrayList;

import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Urls;

import android.content.Context;
import android.content.SharedPreferences;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class SalesProductAdapter extends BaseAdapter {

	String productName;
	String productDesc;
	String productImg;
	int productID;
	int productCatID;
	float partnerPrice;
	float tradePrice;
	float retailPrice;
	float productQuantity;
	public static final String MyPREFERENCES = "MyPrefs" ;
	public static final String TIME = "TimeKey"; 
	SharedPreferences sharedpreferences;
	
	ArrayList<Product> productList = new ArrayList<Product>();
	Context mContext;
	private int hubID;

	int productCategoryID;
	SalesProductAdapter(Context context,int productCategoryID, int hubID) {

		this.mContext = context;
		this.hubID = hubID;
		Product product = new Product(mContext);
		product.open();

		this.productCategoryID = productCategoryID;
		productList = product.getProductsInSales(mContext,productCategoryID, hubID);
//		productList = product.getProductsForCategory(mContext, hubID);

		product.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return productList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		TextView productTitle, productDescription,productIDv,productPrice;
		ImageView productImg;
		public String pPrice;

		ViewHolder(View view) {
			productTitle = (TextView) view.findViewById(R.id.textTitle);
			productDescription = (TextView) view.findViewById(R.id.textDesc);
			productImg = (ImageView) view.findViewById(R.id.imgProduct);
			productIDv = (TextView) view.findViewById(R.id.textID);
			productPrice = (TextView) view.findViewById(R.id.textPrice);

		}
	}

	@Override
	public View getView(int position, View view, ViewGroup productViewGroup) {
		// TODO Auto-generated method stub
		View productView = view;
		ViewHolder vholder = null;


		if (productView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			productView = inflater.inflate(R.layout.single_product,
					productViewGroup, false);
			vholder = new ViewHolder(productView);
			productView.setTag(vholder);
		} else {
			vholder = (ViewHolder) productView.getTag();
		}

		Product tempProduct = productList.get(position);
		vholder.productTitle.setText(tempProduct.productName);
		vholder.productDescription.setText(tempProduct.productDesc);

		String productImageName = tempProduct.productImg;
		Urls urls = new Urls();
		//System.out.println(""+productImageURL);
		String URL = urls.downloadImageUrl + productImageName;
//		String URL = "http://95.85.22.158/upload/product/small/446_572.jpg";
		
		vholder.pPrice = "" + tempProduct.tradePrice + " | " + tempProduct.retailPrice;
		
		vholder.productPrice.setText(vholder.pPrice);
		vholder.productImg.setTag(productImageName);
		vholder.productIDv.setTag(tempProduct);
				
		ImageDownloaderTask imgDownload = new ImageDownloaderTask(mContext,vholder.productImg,URL);
		imgDownload.execute();
		
		return productView;
	}
	
	public void onBackPressed()
	{
		
	   // do nothing to disable back button
	} 

}