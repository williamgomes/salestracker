package com.bscheme.jitasalestracker;
import com.bscheme.jitasalestracker.informations.InformationActivity;
import com.bscheme.popupmenu.Help;
import com.bscheme.sync.SyncProductVersionToUpdate;
import com.bscheme.sync.SyncTransaction;
import com.bscheme.jitasalestracker.inventory.InventoryActivity;
import com.bscheme.jitasalestracker.transactions.TransactionsActivity;

import android.R.raw;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.*;
import android.widget.Button;

public class PopupMenuItems extends AlertDialog implements OnClickListener {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
    public static final String Time = "TimeKey"; 

	Button btnLogout;
	Button btnSetting;
	Button btnTransction;
	Button btnInformation;
	Button btnInventory;
	Button btnHelp;
	Button btnBuy;
	Button btnSale;
	Context mContext;
	public int userTypeID = 0;
	int hubID ;
	int userID;
	AlertDialog.Builder builder;
	
	public PopupMenuItems(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
		builder = new Builder(mContext);
		View view = getLayoutInflater().inflate(R.layout.menu_items, null);
		setView(view);
		
	}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		sharedpref = mContext.getSharedPreferences(MyPREFERENCES, mContext.MODE_PRIVATE);
		if(sharedpref.contains(UserTypeID)){
			userTypeID = sharedpref.getInt(UserTypeID, 0);
		}
		
		hubID = sharedpref.getInt(HubID, 0);
		userID = sharedpref.getInt(UserID, 0);
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		btnLogout = (Button) findViewById(R.id.btn_logout);
		btnLogout.setOnClickListener(this);
		btnTransction = (Button) findViewById(R.id.btn_transactons);
		btnTransction.setOnClickListener(this);
		btnSetting = (Button) findViewById(R.id.btn_Sync);
		btnSetting.setOnClickListener(this);
		btnInformation = (Button) findViewById(R.id.btn_information);
		btnInformation.setOnClickListener(this);
		
		if(userTypeID == 3){ //3 = hubmanager
			btnInventory = (Button) findViewById(R.id.btn_inventory);
			btnInventory.setVisibility(View.VISIBLE);
			btnInventory.setOnClickListener(this);
		}
		
		btnHelp = (Button) findViewById(R.id.btn_help);
		btnHelp.setOnClickListener(this);
		btnSale = (Button) findViewById(R.id.btn_sales);
		btnSale.setOnClickListener(this);
		
		if(userTypeID == 3){ //3 = hubmanager
			btnBuy = (Button) findViewById(R.id.btn_buy);
			btnBuy.setVisibility(View.VISIBLE);
			btnBuy.setOnClickListener(this);
		}
	}
	
	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		switch (view.getId()) {
		case R.id.btn_logout:

			SharedPreferences all = mContext.getSharedPreferences(MyPREFERENCES, mContext.MODE_PRIVATE);
			all.edit().clear().commit();
			
			System.out.println("log out");
			Intent login = new Intent(getContext(), MainActivity.class);
			login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(login);
			finish();
			break;

		case R.id.btn_Sync:
			
			SyncTransaction mSyncTransaction = new SyncTransaction(mContext);
			mSyncTransaction.postJSON(String.valueOf(userID));
			
			SyncProductVersionToUpdate mSyncProductVersionToUpdate = new SyncProductVersionToUpdate(
					mContext);
			mSyncProductVersionToUpdate.postJSON(hubID, userID);
			/*AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create(); //Read Update
			alertDialog.setTitle("");
			alertDialog.setButton(Dialog.BUTTON2, "Post",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							SyncTransaction mSyncTransaction = new SyncTransaction(mContext);
							mSyncTransaction.postJSON(String.valueOf(userID));
							dialog.dismiss();
							finish();
						}
					});

			alertDialog.setButton(Dialog.BUTTON1, "Sync",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							SyncProductVersionToUpdate mSyncProductVersionToUpdate = new SyncProductVersionToUpdate(
									mContext);
							mSyncProductVersionToUpdate.postJSON(hubID, userID);
							dialog.dismiss();
							finish();
						}
					});

			alertDialog.show();  //<-- See This!
*/			
			/*final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.post_dialog);
			dialog.setTitle("Sync to server now");
			
			Button btnPost = (Button) dialog.findViewById(R.id.btn_post);
			Button btnSync = (Button) dialog.findViewById(R.id.btn_sync);
			
			btnPost.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SyncTransaction mSyncTransaction = new SyncTransaction(mContext);
					mSyncTransaction.postJSON(String.valueOf(userID));
					dialog.dismiss();
					finish();
				}
			});
			btnSync.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					SyncProductVersionToUpdate mSyncProductVersionToUpdate = new SyncProductVersionToUpdate(
							mContext);
					mSyncProductVersionToUpdate.postJSON(hubID, userID);
					dialog.dismiss();
					finish();
				}
			});
			
			dialog.show();*/
			break;
		case R.id.btn_transactons:
			Intent trans_intent = new Intent(getContext(),TransactionsActivity.class);
			trans_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(trans_intent);
			finish();
			break;
		case R.id.btn_information:
			Intent info_intent = new Intent(getContext(),InformationActivity.class);
			info_intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(info_intent);
			finish();
			break;
			
		case R.id.btn_help:
			Intent help = new Intent(getContext(),Help.class);
			help.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(help);
			 finish();
			break;
		case R.id.btn_buy:
			Intent distList = new Intent(getContext(),DistributorList.class);
			distList.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(distList);
			 finish();
//			DistributorList distPopupList = new DistributorList(mContext);
//			distPopupList.show();
//			cancel();
			break;	
			
		case R.id.btn_sales:
			Intent sale = new Intent(getContext(),SalesActivity.class);
			sale.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(sale);
			 finish();
			break;
		case R.id.btn_inventory:
			Intent inventory = new Intent(getContext(),InventoryActivity.class);
			inventory.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			getContext().startActivity(inventory);
			 finish();
			break;		
		default:
			break;
		}
		
		
	}
	protected void finish() {
		// TODO Auto-generated method stub
		super.dismiss();
	}
	
}
