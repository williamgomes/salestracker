package com.bscheme.jitasalestracker;

import java.io.IOException;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class LoginAdapter 
{
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DBHelper mDbHelper;

    public LoginAdapter(Context context) 
    {
        this.mContext = context;
        mDbHelper = new DBHelper(mContext);
    }

    public LoginAdapter createDatabase() throws SQLException 
    {
        try 
        {
            mDbHelper.createDataBase();
        } 
        catch (IOException mIOException) 
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public LoginAdapter open() throws SQLException 
    {
        try 
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } 
        catch (SQLException mSQLException) 
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() 
    {
        mDbHelper.close();
    }

	 
	  
	 
	 public int[] loginProcess(String username, String password)
	 {
		 String[] selectArgs = {username, password};
		 String[] colmns = {"hubid","usertype","userid"};
		 int hubID = 0;
		 int userTypeID = 0;
		 int userID = 0;
		 int indexVal1 = 0;
		 int indexVal2 = 0;
		 int indexVal3 = 0;
		 
		 try
	     {
	         Cursor mCur = mDb.query("t_user", colmns, "username=? AND password=?", selectArgs, null, null, null);
	         while (mCur.moveToNext())
	         {
	        	//getting user id from database
	        	indexVal1 = mCur.getColumnIndex("hubid");
	        	hubID = mCur.getInt(indexVal1);
	        	
	            //getting user type id from database
	        	indexVal2 = mCur.getColumnIndex("usertype");
	        	userTypeID = mCur.getInt(indexVal2);
	        	
	        	//getting user type id from database
	        	indexVal3 = mCur.getColumnIndex("userid");
	        	userID = mCur.getInt(indexVal3);
	         }
	        
	         int[] userInfo = {hubID, userTypeID, userID};
	         
//	         Log.v("Login: " , "HUB: " + hubID);
//	         Log.v("Login: " , "Type: " + userTypeID);
//	         Log.v("Login: " , "ID: " + userID);
	         return userInfo;
	     }
	     catch (SQLException mSQLException) 
	     {
	         Log.e(TAG, "getTestData >>"+ mSQLException.toString());
	         throw mSQLException;
	     }
	 }
	
}