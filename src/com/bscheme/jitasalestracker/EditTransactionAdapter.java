package com.bscheme.jitasalestracker;

import java.text.DecimalFormat;
import java.util.ArrayList;

import com.bscheme.util.PicUtils;

import android.R.integer;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;




public class EditTransactionAdapter extends BaseAdapter{
	
	Context context;
	String productName;
    String productDesc;
    float productQuantity;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    String productImg;
	String dateTime;
	int productID;
	int transactionID;
	int transType;
	ArrayList<Transaction> currentTransaction = new ArrayList<Transaction>();
	protected EditTransaction activity;
	
	ViewHolder listHolder = null;
	EditTransactionAdapter(Context context, String dateTime, int transType) {

		this.context = context;
		this.dateTime = dateTime;
		this.transType = transType;
		Transaction trans = new Transaction(context);
		
		trans.open();
		currentTransaction = trans.getCurrentTransaction(dateTime,transType);
		
		trans.close();
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(currentTransaction.size() == 0){
			return 0;
		} else {
			return currentTransaction.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return currentTransaction.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	class ViewHolder {

		TextView productName, productDesc,productUnitPrice,productTotalPrice;
		ImageView productImg;
		EditText productQuantity;
		public TextWatcher textWatcher;
		ImageView btnDelete;

		ViewHolder(View view) {
			productName = (TextView) view.findViewById(R.id.textProductName);
			productDesc = (TextView) view.findViewById(R.id.textProductDesc);
			productUnitPrice = (TextView) view.findViewById(R.id.textProductPrice);
			productTotalPrice = (TextView) view.findViewById(R.id.textProductTotalPrice);
			productQuantity = (EditText) view.findViewById(R.id.txtbxProductQuantity);
			productImg = (ImageView) view.findViewById(R.id.imgview_product);
			btnDelete = (ImageView) view.findViewById(R.id.imgbtnDelete);

		}
	}

	@Override
	public View getView(int position, View view, ViewGroup transactionViewGroup) {
		// TODO Auto-generated method stub
		
		View transactionListRowView = view;
		final ViewHolder finalHolder = listHolder;
		final Transaction tmpTransaction = currentTransaction.get(position);

		if (transactionListRowView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			transactionListRowView = inflater.inflate(R.layout.list_item_edit_transaction,
					transactionViewGroup, false);
			listHolder = new ViewHolder(transactionListRowView);
			transactionListRowView.setTag(listHolder);
			
		} else {
			listHolder = (ViewHolder) transactionListRowView.getTag();
		}
		
		DecimalFormat formater = new DecimalFormat("##.##");
		String productTpMrpPrice = null;
		if(transType == 1){
			productTpMrpPrice = "TK. " + tmpTransaction.productTradePrice + "/" + tmpTransaction.productRetailPrice;
		}else if(transType == 2){
			productTpMrpPrice = "TK. "+""+tmpTransaction.productPartnerPrice+"/" + tmpTransaction.productTradePrice + "/" + tmpTransaction.productRetailPrice;
		}
		
		listHolder.productName.setText(tmpTransaction.productName);
		final String Name = tmpTransaction.productName;
		listHolder.productDesc.setText(tmpTransaction.productDesc);
		listHolder.productImg.setTag(tmpTransaction.productImg);
		listHolder.productTotalPrice.setText(String.valueOf(formater.format(tmpTransaction.productTotalPrice)));
		listHolder.productUnitPrice.setText(productTpMrpPrice);
		
		float pQuant = Float.parseFloat(formater.format(tmpTransaction.productQuantity));
		float quotient = tmpTransaction.productQuantity- (int) (tmpTransaction.productQuantity);		
		float i_fractionValue=quotient * 100; //fraction check
		
		if(i_fractionValue<10){
			listHolder.productQuantity.setText(String.valueOf((int)pQuant));
		}else{
			listHolder.productQuantity.setText(String.valueOf(pQuant));
		}
		
		listHolder.productQuantity.setId(position);
		
		listHolder.productQuantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				int position = v.getId();
				EditText ed = (EditText) v;
				if(position != 0){
					currentTransaction.get(position).productQuantity = Float.parseFloat(ed.getText().toString());
				}
				/*listHolder.productQuantity.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = v.getId();
						EditText ed = (EditText) v;
						if(position != 0){
							currentTransaction.get(position).productQuantity = Float.parseFloat(ed.getText().toString());
						}
					}
				});*/
			}
		});
		
		/*listHolder.productQuantity.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int position = v.getId();
				EditText ed = (EditText) v;
				if(position != 0){
					currentTransaction.get(position).productQuantity = Float.parseFloat(ed.getText().toString());
				}
			}
		});*/
		
//		notifyDataSetChanged();
		listHolder.productQuantity.addTextChangedListener(new CustomTextWatcher(listHolder.productQuantity,currentTransaction.get(position).transactionID,tmpTransaction.productTradePrice));
		final int transactionDetailsID = tmpTransaction.transactionID;
		//delete button to delete item 
		listHolder.btnDelete.setTag(position);
		listHolder.btnDelete.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Log.i("EditTransaction","DeleteID: " + transactionDetailsID);
				Transaction transac = new Transaction(context);
				transac.open();
				boolean status = transac.deleteProduct(transactionDetailsID);
				if(status){
//					Message.message(context, "Product deleted from list.");
				}
				
				int position = (Integer) v.getTag();
				currentTransaction.remove(position);
				notifyDataSetChanged();
			}
		});
		
		PicUtils mPicUtils = new PicUtils();
		Bitmap mBitmap_pLogo = mPicUtils.loadFromCacheFile("product_image", tmpTransaction.productImg);
		listHolder.productImg.setImageBitmap(mBitmap_pLogo);
		return transactionListRowView;
	}
	
	
	
	//created customtextwatcher for edittext in listview
	private class CustomTextWatcher implements TextWatcher {
	    private EditText mEditText;
	    private int productTransID;
	    float productTradePrice;

	    public CustomTextWatcher(EditText e, int productTransID, float productTradePrice) { 
	        this.mEditText = e;
	        this.productTransID = productTransID;
	        this.productTradePrice = productTradePrice;
	    }

	    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	    }

	    public void onTextChanged(CharSequence s, int start, int before, int count) {
	    	/*Log.i("EditTransaction","Start: " + start);
	    	Log.i("EditTransaction","Before: " + before);
	    	Log.i("EditTransaction","Count: " + count);
	    	
	    	
	    	Transaction trans = new Transaction(context);
			trans.open();
			
			String charSeq = s.toString();
			float newQuantity;
			
			//checking if user deleted the whole quantity and pushing 
			if(charSeq == "" || charSeq == null || charSeq == " " || charSeq.length() == 0){
				newQuantity = 0;
			} else {
				newQuantity = Float.parseFloat(charSeq);
			}
			
			Log.i("EditTransaction","NewQuantity: " + newQuantity);
			
			float subTotalValue = newQuantity * productTradePrice;
			boolean status = trans.updateProductQuantity(productTransID, newQuantity,subTotalValue );
			Log.i("EditTransaction","Status: " + status);
//			mEditText.setText(charSeq);
			trans.close();
//			notifyDataSetChanged();
*/	    }

	    public void afterTextChanged(Editable s) {
	    	
	    	if(s.length()>0){
	    		Transaction mTransaction = new Transaction(context);
	    		mTransaction.open();
	    		float subTotalValue =  Float.parseFloat(mEditText.getText().toString()) * productTradePrice;
	    		mTransaction.updateProductQuantity(productTransID, Float.parseFloat(mEditText.getText().toString()), subTotalValue);
	    		mTransaction.close();
	    	}else{
	    		Toast.makeText(context, "Plese Input amount.", Toast.LENGTH_SHORT).show();
	    	}
	    }
	}
}