package com.bscheme.jitasalestracker;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.bscheme.jitasalestracker.BuyProductAdapter.ViewHolder;
import com.bscheme.sliding.ViewPagerAdapter;
import com.bscheme.sync.SyncProductVersionToUpdate;
import com.bscheme.sync.SyncTransaction;
import com.bscheme.util.PicUtils;
import com.jwetherell.quick_response_code.CaptureActivity;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.text.Editable;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class BuyActivity extends Activity implements OnItemClickListener, OnItemLongClickListener {

	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String UserID = "UserIDKey"; 
    public static final String HubID = "HubIDKey"; 
    public static final String UserTypeID = "UserTypeIDKey"; 
    public static final String UserCounterID = "UserCounterIDKey"; 
    public static final String Time = "TimeKey";
    public static final String DistributorID = "DistributorIDKey";
    
    
    String productName;
    String productDesc;
    float productQuantity;
    public String dateTime;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    int productID;
	int transactionID;
    String productImg;
	
	GridView myGrid, productCatGridView2,productCatGridView3,productCatGridView4,productCatGridView5,productCatGridView6,productCatGridViewSP;
	GridView productCatGridViewStar;
	ListView myList;
	Button btnTransaction,btn_popMenu, btnqrcode;
	PopupMenuItems pop;
	int distID = 0;
	int hubID ;
	int userID;
	Context mContext;
	DecimalFormat formater;
	int transTotalItems;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_buy);
		mContext = this;
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		formater = new DecimalFormat("##.##");
		
		if (sharedpref.contains(DistributorID)) {
			distID = sharedpref.getInt(DistributorID, 0);
		}
		
		Log.i("BuyActivity","DistID: " + distID);
		
		pop = new PopupMenuItems(BuyActivity.this);
		btn_popMenu = (Button) findViewById(R.id.btn_popupMenu);
		
		if(distID == 0){
			
			//showing main menu to buyer to choose option
			pop.show();
			
		} else {
		
			btnTransaction = (Button) findViewById(R.id.btn_totalTransaction);
			//btnqrcode = (Button) findViewById(R.id.btn_qrcode);
			
			hubID = sharedpref.getInt(HubID, 0);
			userID = sharedpref.getInt(UserID, 0);
			
			//Message.message(this, "hubID: " + hubID + "userID: " + userID);
			
			LayoutInflater inflater = getLayoutInflater();
			
			final View view_category_special = inflater.inflate (R.layout.category_slideview_special, null);
			final View view_category1 = inflater.inflate (R.layout.category_slideview1, null);
			final View view_category2 = inflater.inflate(R.layout.category_slideview2,null);
			final View view_category3 = inflater.inflate(R.layout.category_slideview3,null);
			final View view_category4 = inflater.inflate(R.layout.category_slideview4,null);
			final View view_category5 = inflater.inflate(R.layout.category_slideview5,null);
			final View view_category6 = inflater.inflate(R.layout.category_slideview6,null);
			final View view_category_star = inflater.inflate(R.layout.category_slideview_star, null);
			
			List<View> viewLists = new ArrayList<View>();
			viewLists.clear();
			viewLists.add(view_category_special);
			viewLists.add(view_category_star);
			viewLists.add(view_category1);
			viewLists.add(view_category2);
			viewLists.add(view_category3);
			viewLists.add(view_category4);
			viewLists.add(view_category5);
			viewLists.add(view_category6);
			
			
			myGrid = (GridView) view_category1.findViewById(R.id.productCatGridView1);
			productCatGridView2 = (GridView) view_category2.findViewById(R.id.productCatGridView2);
			productCatGridView3 = (GridView) view_category3.findViewById(R.id.productCatGridView3);
			productCatGridView4 = (GridView) view_category4.findViewById(R.id.productCatGridView4);
			productCatGridView5 = (GridView) view_category5.findViewById(R.id.productCatGridView5);
			productCatGridView6 = (GridView) view_category6.findViewById(R.id.productCatGridView6);
			productCatGridViewSP = (GridView) view_category_special.findViewById(R.id.productCatGridView_sp);
			productCatGridViewStar = (GridView) view_category_star.findViewById(R.id.productCatGridViewStar);

			Product mProduct = new Product(BuyActivity.this);
			ArrayList<HashMap<String, String>> productCatList = new ArrayList<HashMap<String,String>>();
			productCatList = mProduct.getProductsCategory(BuyActivity.this, hubID);
			
			myGrid.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(0).get("productCategoryId")),hubID,distID));
			productCatGridView2.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(1).get("productCategoryId")),hubID,distID));
			productCatGridView3.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(2).get("productCategoryId")),hubID,distID));
			productCatGridView4.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(3).get("productCategoryId")),hubID,distID));
			productCatGridView5.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(4).get("productCategoryId")),hubID,distID));
			productCatGridView6.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(5).get("productCategoryId")),hubID,distID));
			productCatGridViewSP.setAdapter(new BuyProductAdapter(this,Integer.parseInt(productCatList.get(6).get("productCategoryId")),hubID,distID));
			productCatGridViewStar.setAdapter(new BuyProductAdapter(mContext,1000,hubID,distID));
			
			//Log.v("SalesActivity", "Grid: " + myGrid);
			myGrid.setOnItemClickListener(this);
			productCatGridView2.setOnItemClickListener(this);
			productCatGridView3.setOnItemClickListener(this);
			productCatGridView4.setOnItemClickListener(this);
			productCatGridView5.setOnItemClickListener(this);
			productCatGridView6.setOnItemClickListener(this);
			productCatGridViewSP.setOnItemClickListener(this);
			productCatGridViewStar.setOnItemClickListener(this);
			
			myGrid.setOnItemLongClickListener(this);
			productCatGridView2.setOnItemLongClickListener(this);
			productCatGridView3.setOnItemLongClickListener(this);
			productCatGridView4.setOnItemLongClickListener(this);
			productCatGridView5.setOnItemLongClickListener(this);
			productCatGridView6.setOnItemLongClickListener(this);
			productCatGridViewSP.setOnItemLongClickListener(this);
			productCatGridViewStar.setOnItemLongClickListener(this);
			
			Product product = new Product(BuyActivity.this);
			ArrayList<HashMap<String , String>> listProCat = new ArrayList<HashMap<String,String>>();
			listProCat = product.getProductsCategory(mContext, hubID);
			
				TextView tv_cat_sp = (TextView) view_category_special.findViewById(R.id.id_tvProductCategory_sp);
				tv_cat_sp.setText("Buy: "+""+ listProCat.get(6).get("productCategoryName"));
//				tv_cat_sp.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat1 = (TextView) view_category1.findViewById(R.id.id_tvProductCategory1);
				tv_cat1.setText("Buy: "+"" + listProCat.get(0).get("productCategoryName"));
//				tv_cat1.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat2 = (TextView) view_category2.findViewById(R.id.id_tvProductCategory2);
				tv_cat2.setText("Buy: "+""	+ listProCat.get(1).get("productCategoryName"));
//				tv_cat2.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat3 = (TextView) view_category3.findViewById(R.id.id_tvProductCategory3);
				tv_cat3.setText("Buy: "+""+ listProCat.get(2).get("productCategoryName"));
//				tv_cat3.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat4 = (TextView) view_category4.findViewById(R.id.id_tvProductCategory4);
				tv_cat4.setText("Buy: "+""+ listProCat.get(3).get("productCategoryName"));
//				tv_cat4.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat5 = (TextView) view_category5.findViewById(R.id.id_tvProductCategory5);
				tv_cat5.setText("Buy: "+""+ listProCat.get(4).get("productCategoryName"));
//				tv_cat5.setBackgroundColor(R.color.view_selected);
				
				TextView tv_cat6 = (TextView) view_category6.findViewById(R.id.id_tvProductCategory6);
				tv_cat6.setText("Buy: "+""+ listProCat.get(5).get("productCategoryName"));
//				tv_cat6.setBackgroundColor(R.color.view_selected);
				TextView tv_cat_star = (TextView) view_category_star.findViewById(R.id.id_tvProductCategory_star);
				tv_cat_star.setText("Buy: "+ "Starred Product");
			
//			System.out.println("view lists: " +viewLists.toString());
			ViewPager pager = (ViewPager) findViewById(R.id.pager);
			pager.setAdapter(new ViewPagerAdapter(viewLists));
			pager.setCurrentItem(1);
			
			pager.setOnPageChangeListener(new OnPageChangeListener() {
				

//					
//					trans.close();
				public void onPageSelected(int NoOfViewPage) {
					// TODO Auto-generated method stub
					
					/*Product product = new Product(BuyActivity.this);
					ArrayList<HashMap<String , String>> listProCat = new ArrayList<HashMap<String,String>>();
					listProCat = product.getProductsCategory(mContext, hubID);
					if (NoOfViewPage == 0) {
						TextView tv_cat_sp = (TextView) view_category_special.findViewById(R.id.id_tvProductCategory_sp);
						tv_cat_sp.setText("Buy: "+""+ listProCat.get(6).get("productCategoryName"));
//						tv_cat_sp.setBackgroundColor(R.color.view_selected);
						
					} else if (NoOfViewPage == 1) {
						TextView tv_cat1 = (TextView) view_category1.findViewById(R.id.id_tvProductCategory1);
						tv_cat1.setText("Buy: "+"" + listProCat.get(0).get("productCategoryName"));
//						tv_cat1.setBackgroundColor(R.color.view_selected);
						
					} else if (NoOfViewPage == 2) {
						TextView tv_cat2 = (TextView) view_category2.findViewById(R.id.id_tvProductCategory2);
						tv_cat2.setText("Buy: "+""	+ listProCat.get(1).get("productCategoryName"));
//						tv_cat2.setBackgroundColor(R.color.view_selected);
						
					} else if (NoOfViewPage == 3) {
						TextView tv_cat3 = (TextView) view_category3.findViewById(R.id.id_tvProductCategory3);
						tv_cat3.setText("Buy: "+""+ listProCat.get(2).get("productCategoryName"));
//						tv_cat3.setBackgroundColor(R.color.view_selected);
						
					}else if (NoOfViewPage == 4) {
						TextView tv_cat4 = (TextView) view_category4.findViewById(R.id.id_tvProductCategory4);
						tv_cat4.setText("Buy: "+""+ listProCat.get(3).get("productCategoryName"));
//						tv_cat4.setBackgroundColor(R.color.view_selected);
						
					}else if (NoOfViewPage == 5) {
						TextView tv_cat5 = (TextView) view_category5.findViewById(R.id.id_tvProductCategory5);
						tv_cat5.setText("Buy: "+""+ listProCat.get(4).get("productCategoryName"));
//						tv_cat5.setBackgroundColor(R.color.view_selected);
						
					}else if (NoOfViewPage == 6) {
						TextView tv_cat6 = (TextView) view_category6.findViewById(R.id.id_tvProductCategory6);
						tv_cat6.setText("Buy: "+""+ listProCat.get(5).get("productCategoryName"));
//						tv_cat6.setBackgroundColor(R.color.view_selected);
					}*/
				}
				
				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void onPageScrollStateChanged(int arg0) {
					// TODO Auto-generated method stub

				}
			});
			
		}			
		
		btn_popMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_popMenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				pop = new PopupMenuItems(BuyActivity.this);
				pop.show();
			}
		});

	}	
	
	@Override
	protected void onResume() {
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		
		if (sharedpref.contains(DistributorID)) {
			distID = sharedpref.getInt(DistributorID, 0);
		}
		
		if(distID == 0){
			
			//showing main menu to buyer to choose option
			pop.show();
			
		} else {
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			Transaction trans = new Transaction(BuyActivity.this);
			trans.open();
			
			myList = (ListView) findViewById(R.id.transactionList);
			myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase

			//setting onclick listner to list view
			myList.setOnItemClickListener(new OnItemClickListener() {
				
				@Override
				public void onItemClick(AdapterView<?> adapterview, View view, int i,
						long l) {
					
					Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
					Bundle bundle = new Bundle();
					bundle.putInt("transType", 2);
					editInventory.putExtras(bundle);
					editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(editInventory);
					
				}
			});
			
			float totalTransaction = trans.getTotalAmount(dateTime);
			btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
			trans.close();
			
		}
		
		super.onResume();
	}
	/**
	 * transactionFinal Button click
	 * @param view
	 */
	public void finalTransaction(View view){
		
		Intent finalTransaction = new Intent(BuyActivity.this,TransactionFinal.class);
		Transaction mtTransaction = new Transaction(mContext);
		float finalTotalTransValue = mtTransaction.getTotalAmount(dateTime);
		//initializing bundle to carry values
		Bundle bndl = new Bundle();
		bndl.putInt("transType", 2); //2 = purchase
		bndl.putFloat("finalTotal", finalTotalTransValue);
		bndl.putString("transDate", dateTime);
		bndl.putInt("transTotalItems", transTotalItems);
		//setting bundle inside the activity
		finalTransaction.putExtras(bndl);
		finalTransaction.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(finalTransaction);
		
	}
	
	//function for btnClrLast button
	public void clearLast(View view){
		
		Transaction trans = new Transaction(mContext);
		trans.open();
		
		dateTime = sharedpref.getString(Time, "");
		if(dateTime != ""){
			trans.clearLastProduct(dateTime, 2);
			
			myList = (ListView) findViewById(R.id.transactionList);
			myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
			
			//setting onclick listner to list view
			myList.setOnItemClickListener(new OnItemClickListener() {
				
				@Override
				public void onItemClick(AdapterView<?> adapterview, View view, int i,
						long l) {
					Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
					Bundle bundle = new Bundle();
					bundle.putInt("transType", 2);
					editInventory.putExtras(bundle);
					editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(editInventory);
				}
			});
			float totalTransaction = trans.getTotalAmount(dateTime);
			btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
		} else {
			Message.message(mContext, "Nothing to clear.");
		}
		
		trans.close();
	}
	
	//function for btnClrLast button
	public void clearAll(View view){
		
		Transaction trans = new Transaction(mContext);
		trans.open();
		
		dateTime = sharedpref.getString(Time, "");
		if(dateTime != ""){
			trans.clearAllProduct(dateTime, 2);
			
			myList = (ListView) findViewById(R.id.transactionList);
			myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
			
			//setting onclick listner to list view
			myList.setOnItemClickListener(new OnItemClickListener() {
				
				@Override
				public void onItemClick(AdapterView<?> adapterview, View view, int i,
						long l) {
					Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
					Bundle bundle = new Bundle();
					bundle.putInt("transType", 2);
					editInventory.putExtras(bundle);
					editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(editInventory);
				}
			});
			float totalTransaction = trans.getTotalAmount(dateTime);
			btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
		} else {
			Message.message(mContext, "Nothing to clear.");
		}
		
		trans.close();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub

		switch (item.getItemId()) {
		case R.id.sync:
			Bundle bandle = new Bundle();		
			Intent serciveIntent = new Intent(BuyActivity.this, SyncTransaction.class);
			bandle.putInt("timerValue", 2*60*60*1000);
			serciveIntent.putExtras(bandle);
			
			Intent serciveProductIntent = new Intent(BuyActivity.this, SyncTransaction.class);
			bandle.putInt("timerValue2", 2*60*60*1000);
			serciveProductIntent.putExtras(bandle);
			startService(serciveProductIntent);
			startService(serciveIntent);
			
			break;
		case R.id.sync_stop:
			stopService( new Intent(BuyActivity.this, SyncTransaction.class));
			stopService(new Intent(BuyActivity.this,SyncProductVersionToUpdate.class));
			Toast.makeText(this, "Stoped Sync", Toast.LENGTH_SHORT).show();
			break;

		default:
			break;
		}
		return(super.onOptionsItemSelected(item));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.actions, menu);
		return (super.onCreateOptionsMenu(menu));
	}
	
	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		// TODO Auto-generated method stub
		switch (arg0.getId()) {

		case R.id.productCatGridView1:
			// TODO Auto-generated method stub
			
			//getting product information from viewholder
			ViewHolder vwhldr1 = (ViewHolder) view.getTag();
			Product tempProduct1 = (Product) vwhldr1.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID1 = tempProduct1.productID;
			float partnerPrice1 = tempProduct1.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				
				//UserSelection popup = new UserSelection(BuyActivity.this);
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID1, partnerPrice1, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				trans.close();
			}
		
			break;
		
		case R.id.productCatGridView2:
			// TODO Auto-generated method stub
			
			//getting product information from viewholder
			ViewHolder vwhldr2 = (ViewHolder) view.getTag();
			Product tempProduct2 = (Product) vwhldr2.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID2 = tempProduct2.productID;
			float partnerPrice2 = tempProduct2.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID2, partnerPrice2, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				trans.close();
			}
		
			break;
		case R.id.productCatGridView3:
			// TODO Auto-generated method stub
			
			//getting product information from viewholder
			ViewHolder vwhldr3 = (ViewHolder) view.getTag();
			Product tempProduct3 = (Product) vwhldr3.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID3 = tempProduct3.productID;
			float partnerPrice3 = tempProduct3.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID3, partnerPrice3, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				trans.close();
			}
		
			break;
		case R.id.productCatGridView4:
			//getting product information from viewholder
			ViewHolder vwhldr4 = (ViewHolder) view.getTag();
			Product tempProduct4 = (Product) vwhldr4.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID4 = tempProduct4.productID;
			float partnerPrice4 = tempProduct4.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID4, partnerPrice4, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				trans.close();
			}
		
			break;
		case R.id.productCatGridView5:
			//getting product information from viewholder
			ViewHolder vwhldr5 = (ViewHolder) view.getTag();
			Product tempProduct5 = (Product) vwhldr5.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID5 = tempProduct5.productID;
			float partnerPrice5 = tempProduct5.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID5, partnerPrice5, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+new DecimalFormat("##.##").format(totalTransaction)+" Buy Items");
				
				trans.close();
			}
		
			break;
		case R.id.productCatGridView6:
			
			//getting product information from viewholder
			ViewHolder vwhldr6 = (ViewHolder) view.getTag();
			Product tempProduct6 = (Product) vwhldr6.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			int proID6 = tempProduct6.productID;
			float partnerPrice6 = tempProduct6.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID6, partnerPrice6, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				
				trans.close();
			}
		
			break;
		case R.id.productCatGridView_sp:
			
			//getting product information from viewholder
			vwhldr6 = (ViewHolder) view.getTag();
			tempProduct6 = (Product) vwhldr6.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			proID6 = tempProduct6.productID;
			partnerPrice6 = tempProduct6.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				//UserSelection popup = new UserSelection(BuyActivity.this);
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID6, partnerPrice6, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				
				trans.close();
			}
		
			break;

		case R.id.productCatGridViewStar:
			// TODO Auto-generated method stub
			
			//getting product information from viewholder
			vwhldr1 = (ViewHolder) view.getTag();
			tempProduct1 = (Product) vwhldr1.productIDv.getTag();
//			Product tempProduct = new Product(SalesActivity.this);
			proID1 = tempProduct1.productID;
			partnerPrice1 = tempProduct1.partnerPrice;
			
			dateTime = sharedpref.getString(Time, "");
			if(dateTime == ""){
				SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
		    	dateTime = s.format(new Date());
		    	editor.putString(Time, dateTime);
		    	editor.commit();
			}
			
			
			if (sharedpref.contains(DistributorID)) {
				distID = sharedpref.getInt(DistributorID, 0);
			}
			
			
			//checking if saler already defined or not
			if(distID == 0){ //didnt set user counter id yet, need to start qr code scanner
				
				//UserSelection popup = new UserSelection(BuyActivity.this);
				pop.show();
				
			} else {
			
				Transaction trans = new Transaction(BuyActivity.this, productName, productImg, productDesc, productQuantity, productPartnerPrice, productTradePrice, productRetailPrice, productTotalPrice, productID, transactionID);
				trans.open();
				String status = trans.insertProduct(userID, proID1, partnerPrice1, dateTime,1,2); //2 == purchase
				myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
				
				//setting onclick listner to list view
				myList.setOnItemClickListener(new OnItemClickListener() {
					
					@Override
					public void onItemClick(AdapterView<?> adapterview, View view, int i,
							long l) {
						
						Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
						Bundle bundle = new Bundle();
						bundle.putInt("transType", 2);
						editInventory.putExtras(bundle);
						editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(editInventory);
						
					}
				});
				
				float totalTransaction = trans.getTotalAmount(dateTime);
				btnTransaction.setText("Total TK. "+totalTransaction+" Buy Items");
				
				trans.close();
			}
		
			break;
			
		default:
			break;
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, final View v, int arg2,
			long arg3) {
		// TODO Auto-generated method stub
		PicUtils mPicUtils = new PicUtils();
				
		LayoutInflater inflater = getLayoutInflater();
		final View view = inflater.inflate(R.layout.long_click_from_grid, null);
		final Dialog alertDialog = new Dialog(this); //Read Update
		alertDialog.setTitle("Enter Quantity to Buy");
		alertDialog.setContentView(view);
//		alertDialog.setCancelable(false);
		ViewHolder mViewHolder = (ViewHolder) v.getTag();
		String logoName = (String) mViewHolder.productImg.getTag();
		Bitmap productLogo = mPicUtils.loadFromCacheFile("product_image", logoName);
//		final int proID = Integer.parseInt(mViewHolder.productIDv.getText().toString());
//		final float partnerPrice = Float.parseFloat((String)mViewHolder.productPrice.getText());
		final EditText productQuan = (EditText) view.findViewById(R.id.etxtProductQuantity);
		ImageView iv_pLogo = (ImageView) view.findViewById(R.id.iv_productLogo);
		TextView tv_pname = (TextView) view.findViewById(R.id.tv_pName);
		TextView tv_pDesc = (TextView) view.findViewById(R.id.tv_pDesc);
		TextView tv_pPrice = (TextView) view.findViewById(R.id.tv_pPrice);
		
		if(productLogo != null){
			iv_pLogo.setImageBitmap(productLogo);
		}else{
			iv_pLogo.setBackgroundResource(R.drawable.img_reason_product);
		}
		tv_pname.setText(mViewHolder.productTitle.getText());
		tv_pDesc.setText(mViewHolder.productDescription.getText());
		tv_pPrice.setText(mViewHolder.pPrice);
		Button btn_save = (Button) view.findViewById(R.id.btn_done);
		btn_save.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
						
						sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
						editor = sharedpref.edit();
						
						dateTime = sharedpref.getString(Time, "");
						if(dateTime == ""){
							SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy-hh-mm-ss");
					    	dateTime = s.format(new Date());
					    	editor.putString(Time, dateTime);
					    	editor.commit();
						}
						
						ViewHolder vwhldr = (ViewHolder) v.getTag();
						Product tempProduct = (Product) vwhldr.productIDv.getTag();
//						Product tempProduct = new Product(SalesActivity.this);
						int proID = tempProduct.productID;
						float partnerPrice = tempProduct.partnerPrice;

						
						if(productQuan.getText().toString() == null || productQuan.getText().toString().matches("")){
							
							Toast mToast = Toast.makeText(BuyActivity.this, "Please enter a value", Toast.LENGTH_SHORT);
							mToast.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL, 0, 0);
							mToast.show();
							
						} else {
							Transaction trans = new Transaction(BuyActivity.this);
							
							
							float productQuantity = Float.parseFloat(productQuan.getText().toString());
							
							trans.open();
							trans.insertProduct(userID, proID, partnerPrice, dateTime,productQuantity,2); //2 == purchase
							final float totalTransaction = trans.getTotalAmount(dateTime);
							
							myList.setAdapter(new TransactionAdapter(BuyActivity.this, dateTime,2)); //2 == purchase
							//setting onclick listner to list view
							myList.setOnItemClickListener(new OnItemClickListener() {
								
								@Override
								public void onItemClick(AdapterView<?> adapterview, View view, int i,
										long l) {
									
									Intent editInventory = new Intent(BuyActivity.this, EditTransaction.class);
									Bundle bundle = new Bundle();
									bundle.putInt("transType", 2);
									bundle.putFloat("totalTransValue", totalTransaction);
									editInventory.putExtras(bundle);
									editInventory.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(editInventory);
									
								}
							});
							
							btnTransaction.setText("Total TK. "+new DecimalFormat("##.##").format(totalTransaction)+" Buy Items");
							trans.close();
							alertDialog.dismiss();
						}
						
						
					}
				});

		/*alertDialog.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//						imm.hideSoftInputFromInputMethod(productQuan.getWindowToken(),0);
					}
				}).setOnKeyListener(new OnKeyListener() {
			           @Override
			           public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
			               if(keyCode == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP)
			                   finish();
			               return false;
			           }
			       });*/

		alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		alertDialog.show();
		return false;
	}

}
