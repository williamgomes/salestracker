package com.bscheme.jitasalestracker;

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class TransactionFinalAdapter extends BaseAdapter{
	
	Context mContext;
	String productName;
    String productDesc;
    int productQuantity;
    public String dateTime;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    String productImg;
    int productID;
	int transactionID;
	int transType;
	ArrayList<Transaction> currentTransaction = new ArrayList<Transaction>();
	TextView tv_transTypeName;
	
	TransactionFinalAdapter(Context context, String dateTime, int transType, TextView tv_transTypeName) {

		this.mContext = context;
		this.dateTime = dateTime;
		this.transType = transType;
		this.tv_transTypeName = tv_transTypeName;
		Transaction trans = new Transaction(mContext);
		
		trans.open();
		
		currentTransaction = trans.getCurrentTransaction(dateTime,transType);
		
		trans.close();
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(currentTransaction.size() == 0){
			return 0;
		} else {
			return currentTransaction.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return currentTransaction.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	class ViewHolder {

		TextView productName, productUnitPrice,productQuantity,productPrice;

		float totalTransQuantity = 0;
		ViewHolder(View view) {
			productName = (TextView) view.findViewById(R.id.txtProductName);
			productUnitPrice = (TextView) view.findViewById(R.id.txtunitPrice);
			productQuantity = (TextView) view.findViewById(R.id.txtProductQuantity);
			productPrice = (TextView) view.findViewById(R.id.txtProductSubTotal);

		}
	}
	
	

	@Override
	public View getView(int position, View view, ViewGroup transactionViewGroup) {
		// TODO Auto-generated method stub
		
		View transactionList = view;
		ViewHolder listHolder = null;


		if (transactionList == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			transactionList = inflater.inflate(R.layout.list_item_final_transaction,
					transactionViewGroup, false);
			listHolder = new ViewHolder(transactionList);
			transactionList.setTag(listHolder);
		} else {
			listHolder = (ViewHolder) transactionList.getTag();
		}
		
		Transaction tmpTransaction = currentTransaction.get(position);
		
		String productTpMrpPrice = "TK." + (tmpTransaction.productTotalPrice / tmpTransaction.productQuantity);
		listHolder.productQuantity.setText(String.valueOf(tmpTransaction.productQuantity));
		listHolder.productName.setText(tmpTransaction.productName);
		listHolder.productUnitPrice.setText(productTpMrpPrice);
		listHolder.productPrice.setText(String.valueOf(tmpTransaction.productTotalPrice));
		
		listHolder.totalTransQuantity = listHolder.totalTransQuantity + Float.parseFloat((String) listHolder.productQuantity.getText());
		
		if (transType == 2) {
			tv_transTypeName.setText("" +(int)listHolder.totalTransQuantity+ " - Items Bought");
		}else if (transType == 1) {
			tv_transTypeName.setText("" +(int)listHolder.totalTransQuantity+ " - Items Sold");
		}
		return transactionList;
	}
	
	
	
	
	
}