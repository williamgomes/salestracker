package com.bscheme.jitasalestracker;
import com.bscheme.jitasalestracker.DistributorListAdapter.ViewHolder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.*;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;


public class DistributorList extends Activity {
	
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String HubID = "HubIDKey"; 
	public static final String DistributorID = "DistributorIDKey";
	
	Context mContext;
	ListView distListView;
	Button btn_PopUpMenu;

//	protected DistributorList(Context context) {
//		super(context);
//		// TODO Auto-generated constructor stub
//		mContext = context;
//		View view = getLayoutInflater().inflate(R.layout.distributor_list, null);
//		setView(view);
//	}
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar
	    this.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    //Remove notification bar
	    this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.distributor_list);
		
		sharedpref = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		int hubID = 0;
		
		if(sharedpref.contains(HubID)){
			hubID = sharedpref.getInt(HubID, 0);
		}
		btn_PopUpMenu = (Button) findViewById(R.id.btn_popupMenu);
		btn_PopUpMenu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				PopupMenuItems mPopupMenuItems = new PopupMenuItems(DistributorList.this);
				mPopupMenuItems.show();
			}
		});
		distListView = (ListView) findViewById(R.id.lvDistributor);
		distListView.setAdapter(new DistributorListAdapter(this, hubID));
		
		distListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterview, View view, int i,
					long l) {
				// TODO Auto-generated method stub
				ViewHolder distvHolder = (ViewHolder) view.getTag();
				Distributors tmpDist = (Distributors) distvHolder.txtDistID.getTag();
				int distID = tmpDist.distributorID;
				
				Log.i("DistributorList","DistID: " + distID);
				
				editor.putInt(DistributorID, distID);
				editor.commit();
				
				Intent buyActivity = new Intent(DistributorList.this,BuyActivity.class);
				buyActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(buyActivity);
			}
		});
		
	}


//	@Override
//	public void onClick(View arg0) {
//		// TODO Auto-generated method stub
//		
//	}
	
}