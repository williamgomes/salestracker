package com.bscheme.jitasalestracker;

import java.text.DecimalFormat;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TransactionAdapter extends BaseAdapter{
	
	Context mContext;
	String productName;
    String productDesc;
    float productQuantity;
    public String dateTime;
    float productPartnerPrice;
    float productTradePrice;
    float productRetailPrice;
    float productTotalPrice;
    int productID;
	int transactionID;
	int transType;
    String productImg;
	ArrayList<Transaction> currentTransaction = new ArrayList<Transaction>();
	
	
	TransactionAdapter(Context context, String dateTime, int transType) {

		this.mContext = context;
		this.dateTime = dateTime;
		this.transType = transType;
		Transaction trans = new Transaction(mContext);
		
		trans.open();
		
		currentTransaction = trans.getCurrentTransaction(dateTime,transType);
		
		trans.close();
		
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(currentTransaction.size() == 0){
			return 0;
		} else {
			return currentTransaction.size();
		}
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return currentTransaction.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	
	class ViewHolder {

		TextView productName, productDesc,productQuantity,productTotalPrice;

		float totalTransQuantity = 0;
		ViewHolder(View view) {
			productName = (TextView) view.findViewById(R.id.tv_pName);
			productDesc = (TextView) view.findViewById(R.id.tv_pDesc);
			productQuantity = (TextView) view.findViewById(R.id.tv_pQuantity);
			productTotalPrice = (TextView) view.findViewById(R.id.tv_pSubTotal);

		}
	}
	
	

	@Override
	public View getView(int position, View view, ViewGroup transactionViewGroup) {
		// TODO Auto-generated method stub
		
		View transactionList = view;
		ViewHolder listHolder = null;


		if (transactionList == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			transactionList = inflater.inflate(R.layout.list_item_transaction,
					transactionViewGroup, false);
			listHolder = new ViewHolder(transactionList);
			transactionList.setTag(listHolder);
		} else {
			listHolder = (ViewHolder) transactionList.getTag();
		}
		
		Transaction tmpTransaction = currentTransaction.get(position);
		DecimalFormat formater = new DecimalFormat("##.##");
		
//		listHolder.productQuantity.setText(String.valueOf(tmpTransaction.productQuantity));
		listHolder.productName.setText(tmpTransaction.productName);
		listHolder.productDesc.setText(tmpTransaction.productDesc);
		listHolder.productTotalPrice.setText(String.valueOf(formater.format(tmpTransaction.productTotalPrice)));
		float pQuant = Float.parseFloat(formater.format(tmpTransaction.productQuantity));
		listHolder.totalTransQuantity = listHolder.totalTransQuantity + pQuant;
		float quotient = tmpTransaction.productQuantity- (int) (tmpTransaction.productQuantity);
		float i_fractionValue= quotient * 100; //fraction check
//		System.out.println("pQuant="+pQuant+"quotient=="+quotient+"i_fractionValue====== " +i_fractionValue);
		if((int)i_fractionValue<10){
			listHolder.productQuantity.setText(String.valueOf((int)pQuant));
		}else{
			listHolder.productQuantity.setText(String.valueOf(pQuant));
		}
		
		return transactionList;
	}
	
	
	
	
	
}