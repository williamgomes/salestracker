package com.bscheme.jitasalestracker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.transform.Templates;

import com.bscheme.jitasalestracker.inventory.Inventory;
import com.bscheme.jitasalestracker.inventory.Inventory.ProductInfo;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Product {

	protected static final String TAG = "Product";

	private final Context mContext;
	private SQLiteDatabase mDb;
	private DBHelper mDbHelper;

	public String productName;
	public String productDesc;
	public String productImg;
	public int productCatID;
	public int productID;
	public float tradePrice;
	public float partnerPrice;
	public float retailPrice;
	public float productQuantity;

	public Product(Context context, String productName, String productDesc,
			String productImg, int productCatID, int productID,
			float partnerPrice, float tradePrice, float retailPrice,
			float productQuantity) {

		this.mContext = context;
		this.productName = productName;
		this.productDesc = productDesc;
		this.productImg = productImg;
		this.productCatID = productCatID;
		this.productID = productID;
		this.tradePrice = tradePrice;
		this.partnerPrice = partnerPrice;
		this.retailPrice = retailPrice;
		this.productQuantity = productQuantity;
		mDbHelper = new DBHelper(mContext);

	}

	public Product(Context c) {
		this.mContext = c;
		mDbHelper = new DBHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();
	}

	public Product open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDbHelper.close();
			mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	public ArrayList<Product> getProductsInBuy(Context mContext, int productCategoryID,int hubID, int manufID) {

//		System.out.println(""+hubID+" = "+manufID);
		String rawQuery = "SELECT t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_product.categoryid AS categoryid, t_product.productimage AS productimg FROM t_product WHERE t_product.categoryid = ? AND t_product.manufid = ?";
		String[] selAgrs = { String.valueOf(productCategoryID),String.valueOf(manufID) };
		ArrayList<Product> productList = new ArrayList<Product>();

		try {
			Cursor Crsr = mDb.rawQuery(rawQuery, selAgrs);
			while (Crsr.moveToNext()) {
				Product tmpProduct = new Product(mContext, Crsr.getString(Crsr
						.getColumnIndex("productname")), Crsr.getString(Crsr
						.getColumnIndex("productdesc")), Crsr.getString(Crsr
						.getColumnIndex("productimg")), Crsr.getInt(Crsr
						.getColumnIndex("categoryid")), Crsr.getInt(Crsr
						.getColumnIndex("productid")), Crsr.getFloat(Crsr
						.getColumnIndex("partnerprice")), Crsr.getFloat(Crsr
						.getColumnIndex("tradeprice")), Crsr.getFloat(Crsr
						.getColumnIndex("retailprice")), 0);
				productList.add(tmpProduct);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;

	}

	public ArrayList<ProductInfo> getAllProductsInventory(Context mContext,
			int hubID) {

		String rawQuery = "SELECT t_hubproduct.starproduct,h.hubname,t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg, t_hubproduct.quantityavailable AS stockquantity FROM t_product,t_hubproduct,t_hub h WHERE t_product.productid=t_hubproduct.productid AND h.hubid = '"+hubID+"' AND t_hubproduct.hubid=?";
		String[] selAgrs = { String.valueOf(hubID) };
		ArrayList<ProductInfo> productList = new ArrayList<ProductInfo>();

		try {
			Cursor Crsr = mDb.rawQuery(rawQuery, selAgrs);
			while (Crsr.moveToNext()) {
				Inventory inv = new Inventory();
				ProductInfo tmpProduct = inv.new ProductInfo();
				tmpProduct.hubName = Crsr.getString(Crsr.getColumnIndex("hubname"));
				tmpProduct.productName = Crsr.getString(Crsr.getColumnIndex("productname"));
				tmpProduct.productDesc = Crsr.getString(Crsr.getColumnIndex("productdesc"));
				tmpProduct.productImg = Crsr.getString(Crsr.getColumnIndex("productimg"));
				tmpProduct.productCatID = Crsr.getInt(Crsr.getColumnIndex("categoryid")); 
				tmpProduct.productID = Crsr.getInt(Crsr.getColumnIndex("productid"));
				tmpProduct.partnerPrice = Crsr.getFloat(Crsr.getColumnIndex("partnerprice"));
				tmpProduct.tradePrice = Crsr.getFloat(Crsr.getColumnIndex("tradeprice"));
				tmpProduct.retailPrice = Crsr.getFloat(Crsr.getColumnIndex("retailprice"));
				tmpProduct.stockQuantity = Crsr.getFloat(Crsr.getColumnIndex("stockquantity"));
				String isChecked = Crsr.getString(Crsr.getColumnIndex("starproduct"));
				if(isChecked.equals("false")){
					tmpProduct.selected = false;
				}else if(isChecked.equals("true")){
					tmpProduct.selected = true;
				}
				
				
				productList.add(tmpProduct);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;

	}

	public ArrayList<Product> getProductsForCategory(Context mContext,
			int hubID) {

		String[] selAgrs = { String.valueOf(hubID) };
		ArrayList<Product> productList = new ArrayList<Product>();

		try {

			// String rawQuery =
			// "SELECT t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg FROM t_product,t_hubproduct WHERE t_product.productid=t_hubproduct.productid AND t_hubproduct.hubid=?";
			// String[] selAgrs = { String.valueOf(hubID) };

			String getProductInfo = "SELECT t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg FROM t_product,t_hubproduct WHERE t_product.productid=t_hubproduct.productid AND t_product.categoryid = '4' AND t_hubproduct.hubid=?";
			Cursor Crsr = mDb.rawQuery(getProductInfo, selAgrs);
			while (Crsr.moveToNext()) {
				Product tmpProduct = new Product(mContext, Crsr.getString(Crsr
						.getColumnIndex("productname")), Crsr.getString(Crsr
						.getColumnIndex("productdesc")), Crsr.getString(Crsr
						.getColumnIndex("productimg")), Crsr.getInt(Crsr
						.getColumnIndex("categoryid")), Crsr.getInt(Crsr
						.getColumnIndex("productid")), Crsr.getFloat(Crsr
						.getColumnIndex("partnerprice")), Crsr.getFloat(Crsr
						.getColumnIndex("tradeprice")), Crsr.getFloat(Crsr
						.getColumnIndex("retailprice")), 0);
				productList.add(tmpProduct);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;

	}

	public ArrayList<HashMap<String, String>> getProductsCategory(Context mContext,	int hubID) {

		String[] selAgrs = { String.valueOf(hubID) };
		ArrayList<HashMap<String, String>> productList = new ArrayList<HashMap<String, String>>();

		try {

			// String rawQuery =
			// "SELECT t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg FROM t_product,t_hubproduct WHERE t_product.productid=t_hubproduct.productid AND t_hubproduct.hubid=?";
			// String[] selAgrs = { String.valueOf(hubID) };

			/*String getProductInfo = "SELECT categoryid FROM t_hubproduct WHERE hubid=? ORDER BY categoryid ASC";
			Cursor Crsr = mDb.rawQuery(getProductInfo, selAgrs);
			while (Crsr.moveToNext()) {
				
						int productCategoryId = Crsr.getInt(Crsr.getColumnIndex("categoryid"));
						int temp = -1 ;
						if(productCategoryId != temp){
							temp = productCategoryId;
							
							
						}
						
			}*/
			
			Cursor cursor = mDb.rawQuery("SELECT categoryid, ecategoryname FROM t_category", null);
			
			String productCategoryName = null ;
			while (cursor.moveToNext()) {
				productCategoryName = cursor.getString(cursor.getColumnIndex("ecategoryname"));	
				HashMap<String, String> map = new HashMap<String, String>();
				map.put("productCategoryName", productCategoryName);
				map.put("productCategoryId", String.valueOf(cursor.getInt(cursor.getColumnIndex("categoryid"))));
//				System.out.println(""+productCatID+"=" +productCategoryName);
				productList.add(map);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;

	}
	
	public List<Hub> getHubInfo(int hubId){
		
		List<Hub> listHub = new ArrayList<Hub>();
		
		String query_getHubInfo = "SELECT * FROM t_hub WHERE hubid = '"+hubId+"'";
		Cursor cursor_HubInfo = mDb.rawQuery(query_getHubInfo, null);
		while (cursor_HubInfo.moveToNext()) {
			Hub mHub = new Hub();
			
			mHub.hubName = cursor_HubInfo.getString(cursor_HubInfo.getColumnIndex("hubname"));
			listHub.add(mHub);
		}
		return listHub;
	}

	public void updateStarProduct(int hubID, int productID, boolean checked) {
		// TODO Auto-generated method stub
		
//		System.out.println("hub: " +hubID+"pro:"+productID+"checked: "+checked);
		String query_updateStarPro = "UPDATE t_hubproduct SET starproduct = ? WHERE productid = '"+productID+"' AND hubid = '"+hubID+"'";
		mDb.execSQL(query_updateStarPro, new String[]{"" + String.valueOf(checked)});
	}

	public ArrayList<Product> getStarredProducts(Context mContext,boolean isChecked, int hubID) {
		// TODO Auto-generated method stub
		System.out.println("id star : " +isChecked + "= " +hubID);
		
//		String rawQuery = "SELECT * FROM t_product,t_hubproduct WHERE t_product.productid = t_hubproduct.productid AND t_hubproduct.starproduct = '"+isChecked+"' AND t_hubproduct.hubid=?";
//		String[] selAgrs = {String.valueOf(hubID) };
		String rawQuery = "SELECT t_hubproduct.starproduct,h.hubname,t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg, t_hubproduct.quantityavailable AS stockquantity FROM t_product,t_hubproduct,t_hub h WHERE t_product.productid=t_hubproduct.productid AND h.hubid = '"+hubID+"' AND t_hubproduct.hubid=? AND t_hubproduct.starproduct = '"+isChecked+"'";
		String[] selAgrs = { String.valueOf(hubID) };
		ArrayList<Product> productList = new ArrayList<Product>();

		try {
			Cursor Crsr = mDb.rawQuery(rawQuery, selAgrs);
			while (Crsr.moveToNext()) {
				Inventory inv = new Inventory();
				Product tmpProduct = new Product(mContext);
//				tmpProduct.hubName = Crsr.getString(Crsr.getColumnIndex("hubname"));
				tmpProduct.productName = Crsr.getString(Crsr.getColumnIndex("productname"));
				tmpProduct.productDesc = Crsr.getString(Crsr.getColumnIndex("productdesc"));
				tmpProduct.productImg = Crsr.getString(Crsr.getColumnIndex("productimg"));
				tmpProduct.productCatID = Crsr.getInt(Crsr.getColumnIndex("categoryid")); 
				tmpProduct.productID = Crsr.getInt(Crsr.getColumnIndex("productid"));
				tmpProduct.partnerPrice = Crsr.getFloat(Crsr.getColumnIndex("partnerprice"));
				tmpProduct.tradePrice = Crsr.getFloat(Crsr.getColumnIndex("tradeprice"));
				tmpProduct.retailPrice = Crsr.getFloat(Crsr.getColumnIndex("retailprice"));
//				tmpProduct.stockQuantity = Crsr.getFloat(Crsr.getColumnIndex("stockquantity"));
//				String isChecked = Crsr.getString(Crsr.getColumnIndex("starproduct"));
				/*if(isChecked.equals("false")){
					tmpProduct.selected = false;
				}else if(isChecked.equals("true")){
					tmpProduct.selected = true;
				}*/
				
				
				productList.add(tmpProduct);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;
	}
	
	public int getManufIdForDistributor(int distId){
		int manufID = 0;
		System.out.println("dist id to get manuf id :" +distId);
		String[] argD = {String.valueOf(distId) };
		String getDist = "SELECT * FROM t_distributor where distid = ?";
		Cursor dCur = mDb.rawQuery(getDist, argD);

		while (dCur.moveToNext()) {
//			user.fullName = dCur.getString(dCur.getColumnIndex("distname"));
//			user.mobileNo = dCur.getString(dCur.getColumnIndex("edistname"));
			manufID = dCur.getInt(dCur.getColumnIndex("manufid")) ;
//			System.out.println("manufId = " +manufID);
		}
		return manufID;
	}

	public ArrayList<Product> getProductsInSales(Context mContext2,
			int productCategoryID, int hubID) {
		// TODO Auto-generated method stub
		String rawQuery = "SELECT t_product.productid AS productid, t_product.eproductname1 AS productname, t_product.eproductname2 AS productdesc, t_product.pp AS partnerprice, t_product.tp AS tradeprice, t_product.mrp AS retailprice, t_hubproduct.categoryid AS categoryid, t_product.productimage AS productimg FROM t_product,t_hubproduct WHERE t_product.productid=t_hubproduct.productid AND t_product.categoryid = ? AND t_hubproduct.hubid=?";
		String[] selAgrs = { String.valueOf(productCategoryID),String.valueOf(hubID)};
		ArrayList<Product> productList = new ArrayList<Product>();

		try {
			Cursor Crsr = mDb.rawQuery(rawQuery, selAgrs);
			while (Crsr.moveToNext()) {
				Product tmpProduct = new Product(mContext, Crsr.getString(Crsr
						.getColumnIndex("productname")), Crsr.getString(Crsr
						.getColumnIndex("productdesc")), Crsr.getString(Crsr
						.getColumnIndex("productimg")), Crsr.getInt(Crsr
						.getColumnIndex("categoryid")), Crsr.getInt(Crsr
						.getColumnIndex("productid")), Crsr.getFloat(Crsr
						.getColumnIndex("partnerprice")), Crsr.getFloat(Crsr
						.getColumnIndex("tradeprice")), Crsr.getFloat(Crsr
						.getColumnIndex("retailprice")), 0);
				productList.add(tmpProduct);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productList;
	}
}