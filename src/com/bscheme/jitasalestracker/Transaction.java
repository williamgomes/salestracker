package com.bscheme.jitasalestracker;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import com.bscheme.util.DownloadAndSave;
import com.bscheme.util.Sales;
import com.bscheme.util.Sales.TransactionListItemDetail;
import com.bscheme.util.UserContent;
import com.bscheme.util.Sales.SalesBy;
import com.bscheme.util.Sales.SalesTo;
import com.bscheme.util.Sales.TransactionsDetail;
import com.bscheme.util.Sales.YesterdaySalesInfo;
import com.google.zxing.common.StringUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Transaction {

	protected static final String TAG = "Transaction";

	private final Context mContext;
	private SQLiteDatabase mDb;
	private DBHelper mDbHelper;
	String productName;
	String productDesc;
	float productQuantity;
	float productPartnerPrice;
	float productTradePrice;
	float productRetailPrice;
	float productTotalPrice;
	int productID;
	int transactionID;
	String productImg;
	String custName;
	String mobileNo;

	int userID;

	public Transaction(Context context, String productName, String productImg,
			String productDesc, float productQuantity,
			float productPartnerPrice, float productTradePrice,
			float productRetailPrice, float productTotalPrice, int productID,
			int transactionID) {

		this.productDesc = productDesc;
		this.productImg = productImg;
		this.productName = productName;
		this.productQuantity = productQuantity;
		this.productPartnerPrice = productPartnerPrice;
		this.productTradePrice = productTradePrice;
		this.productRetailPrice = productRetailPrice;
		this.productTotalPrice = productTotalPrice;
		this.productID = productID;
		this.transactionID = transactionID;
		this.mContext = context;
		mDbHelper = new DBHelper(mContext);

	}

	public Transaction(Context context) {

		this.mContext = context;
		mDbHelper = new DBHelper(mContext);
		mDb = mDbHelper.getWritableDatabase();

	}

	public Transaction(Context context, int userID, String custName,
			String mobileNo) {

		this.mContext = context;
		this.custName = custName;
		this.mobileNo = mobileNo;
		this.userID = userID;
		mDbHelper = new DBHelper(mContext);

	}

	public Transaction open() throws SQLException {
		try {
			mDbHelper.openDataBase();
			mDbHelper.close();
			mDb = mDbHelper.getReadableDatabase();
		} catch (SQLException mSQLException) {
			Log.e(TAG, "open >>" + mSQLException.toString());
			throw mSQLException;
		}
		return this;
	}

	public void close() {
		mDbHelper.close();
	}

	
	
	
	/*
	 * Inserting a new product or updating an existing product in
	 * transactiondetail table
	 */
	public String insertProduct(int userID, int productID, float price,
			String dataTime, float productQuantity, int transTypeID) {

		// SimpleDateFormat s = new SimpleDateFormat("ddMMyyyyhhmmss");
		// String format = s.format(new Date());

		
		Calendar c = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
		String currentDateTime = sdf.format(c.getTime());

		String status = "";
		String queryCheckDB = "SELECT * FROM t_transactiondetail WHERE productid="
				+ productID + " AND transactiondatetime='" + dataTime + "'";
		String[] selAgrs = { String.valueOf(productID), dataTime };

		try {
			Cursor cursorCheckProduct = mDb.rawQuery(queryCheckDB, null);
			int count = cursorCheckProduct.getCount();
			Log.v("TransQuery:", "Count: " + count);

			if (count == 0) { /*
							 * this product didnt added to transaction before,
							 * need to add it
							 */

				try {
					String queryInsertProduct = "INSERT INTO t_transactiondetail (transactiontype,updated,transactiondatetime,productid,quantity,price,subtotalvalue,status) values('"
							+ transTypeID
							+ "','"
							+ currentDateTime
							+ "','"
							+ dataTime
							+ "',"
							+ productID
							+ ",'"
							+ productQuantity
							+"','"
							+ price
							+ "','"
							+ price*productQuantity
							+ "','false');";
					Log.v("TransQuery:", "InsertQuery: " + queryInsertProduct);
					mDb.execSQL(queryInsertProduct);
					status = "New Product Added";

					System.out.println("quantity insert  :" + productQuantity);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					// e.printStackTrace();
					// Message.message(mContext,
					// "Product Add Failed. Exception: " + e.getMessage());
					status = "Product Failed: " + e.getMessage();
				}

			} else { /* this product already exist in record, need to update it */

				float curProductQuantity = 0;
				float newProductQuantity = 0;
				float newProductSubTotalPrice = 0;

				while (cursorCheckProduct.moveToNext()) {
					curProductQuantity = cursorCheckProduct
							.getInt(cursorCheckProduct
									.getColumnIndex("quantity"));

					/* calculating new product quantity */
					newProductQuantity = curProductQuantity + productQuantity;
					newProductSubTotalPrice = price * newProductQuantity;
					
					System.out.println("new price : " +newProductSubTotalPrice);
				}

				// trying to update existing product in database
				try {
					String queryInsertProduct = "UPDATE t_transactiondetail SET quantity="
							+ newProductQuantity
							+ ", subtotalvalue='"
							+ newProductSubTotalPrice
							+ "', updated='"
							+ currentDateTime
							+ "' WHERE productid="
							+ productID
							+ " AND transactiondatetime='"
							+ dataTime + "';";
					Log.v("TransQuery:", "UpdateQuery: " + queryInsertProduct);
					mDb.execSQL(queryInsertProduct);
					status = "Product Updated";
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					status = "Product Failed: " + e.getMessage();
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			status = "Product Failed: " + e.getMessage();
		}
		return status;
	}
	
	
	
	

	// getting current transaction array from database
	public ArrayList<Transaction> getCurrentTransaction(String dateTime, int transType) {

		ArrayList<Transaction> trans = new ArrayList<Transaction>();

		String queryGetTransaction = "SELECT * FROM t_transactiondetail, t_product WHERE t_transactiondetail.productid=t_product.productid AND transactiondatetime='"
				+ dateTime + "' AND transactiontype="+ transType +" ORDER BY updated DESC";

		try {
			Cursor mCur = mDb.rawQuery(queryGetTransaction, null);

			while (mCur.moveToNext()) {
				int index1 = mCur.getColumnIndex("eproductname1");
				int index2 = mCur.getColumnIndex("eproductname2");
				int index3 = mCur.getColumnIndex("quantity");
				int index4 = mCur.getColumnIndex("subtotalvalue");
				int index5 = mCur.getColumnIndex("pp");
				int index6 = mCur.getColumnIndex("tp");
				int index7 = mCur.getColumnIndex("mrp");
				int index8 = mCur.getColumnIndex("productimage");
				int index9 = mCur.getColumnIndex("productid");
				int index10 = mCur.getColumnIndex("transactionid");

				Transaction tmpTrans = new Transaction(mContext,
						mCur.getString(index1), mCur.getString(index8),
						mCur.getString(index2), mCur.getInt(index3),
						mCur.getFloat(index5), mCur.getFloat(index6),
						mCur.getFloat(index7), mCur.getFloat(index4),
						mCur.getInt(index9), mCur.getInt(index10));
				trans.add(tmpTrans);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return trans;

	}
	
	

	// getting total transaction amount from database
	public float getTotalAmount(String dateTime) {

		String queryGetTransaction = "SELECT SUM(subtotalvalue) AS TotalPrice FROM t_transactiondetail WHERE transactiondatetime='"
				+ dateTime + "'";
		float totalAmount = 0;

		try {
			Cursor mCur = mDb.rawQuery(queryGetTransaction, null);

			while (mCur.moveToNext()) {
				int index1 = mCur.getColumnIndex("TotalPrice");

				totalAmount = mCur.getFloat(index1);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return totalAmount;
	}
	
	

	// saving transaction details in database
	public boolean saveTransaction(int userID, int counterPart,
			int transactionType, String dateTime, int hubID) {

		boolean status = false;
		long transactionID = 0;
		float totalAmount = getTotalAmount(dateTime);
		float lat = 0;
		float lon = 0;
		float locAcc = 0;
		String newDateTime = null;

		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		newDateTime = s.format(new Date());
		Log.i("TransactionDate", "DateTime: " + newDateTime);

		ContentValues values = new ContentValues();
		values.put("userid", userID);
		values.put("counterpartid", counterPart);
		values.put("datetime", newDateTime);
		values.put("transactiontype", transactionType);
		values.put("totalvalue", totalAmount);
		values.put("lat", lat);
		values.put("lon", lon);
		values.put("locacc", locAcc);
		values.put("status", "false");

		try {

			// saving transaction information inside table and returning row id
			transactionID = mDb.insert("t_transaction", null, values);

			// searching for transaction details record
			String queryInsertProduct = "UPDATE t_transactiondetail SET transactionid="
					+ transactionID
					+ " WHERE transactiondatetime='"
					+ dateTime
					+ "';";
			mDb.execSQL(queryInsertProduct);

			try {

				// getting all products id and quantity from transactiondetail
				// table
				String queryAllCurrentTransaction = "SELECT productid,quantity FROM t_transactiondetail WHERE transactiondatetime='"
						+ dateTime + "' ORDER BY transactionid DESC";
				Cursor mCursor = mDb.rawQuery(queryAllCurrentTransaction, null);

				while (mCursor.moveToNext()) {

					int index1 = mCursor.getColumnIndex("productid");
					int index2 = mCursor.getColumnIndex("quantity");

					// getting product id and transaction quantity
					int productID = mCursor.getInt(index1);
					float productTransQuantity = mCursor.getFloat(index2);

					try {

						// getting current stock quantity for each product from
						// hubproduct table
						String queryGetProduct = "SELECT quantityavailable FROM t_hubproduct WHERE hubid="
								+ hubID + " AND productid=" + productID;
						Cursor mCur = mDb.rawQuery(queryGetProduct, null);
						while (mCur.moveToNext()) {

							// calculating new stock quantity from old stock and
							// transaction quantity
							int indx1 = mCur
									.getColumnIndex("quantityavailable");
							float currentQuantity = mCur.getFloat(indx1);

							float newQuantity = 0;

							if (transactionType == 1) {
								newQuantity = currentQuantity
										- productTransQuantity;
							} else if (transactionType == 2) {
								newQuantity = currentQuantity
										+ productTransQuantity;
							}

							try {

								// updating new stock in database
								String queryUpdateStock = "UPDATE t_hubproduct SET quantityavailable="
										+ newQuantity
										+ " WHERE hubid="
										+ hubID
										+ " AND productid=" + productID;
								mDb.execSQL(queryUpdateStock);
								status = true;
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								status = false;
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						status = false;
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				status = false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	// updating product quantity
	public boolean updateProductQuantity(int transactionDetailsID,
			float newQuantity, float subTotalValue) {

		boolean status;
		try {
			// searching for transaction details record
			String updateQuantity = "UPDATE t_transactiondetail SET quantity="
					+ newQuantity + ", subtotalvalue = '"+subTotalValue+"' WHERE transactiondid='"
					+ transactionDetailsID + "';";
			mDb.execSQL(updateQuantity);
			Log.i("Transaction", "UpdateQuery: " + updateQuantity +"sub total = "+subTotalValue);
			status = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	// function to get average sales quantity from database
	public float getAvgQuantitySold(int hubID, int productID) {

		float avgQuan = 0;

		Calendar dateTimeNow = Calendar.getInstance();
		Calendar dateTimeLater = Calendar.getInstance();

		// going back 28 days from current date time
		dateTimeLater.add(Calendar.DATE, -28);

		// converting calender to simpledateformat
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat Format = new SimpleDateFormat("yyyy-MM-dd");
		String nowDateTime = dateFormat.format(dateTimeNow.getTime());
		String prevDateTime = dateFormat.format(dateTimeLater.getTime());

		String lastTransDate = nowDateTime;

		String getAllUsers = "SELECT t_transaction.datetime FROM t_user,t_transaction WHERE t_user.hubid="
				+ hubID
				+ " AND t_user.userid=t_transaction.userid ORDER BY datetime ASC LIMIT 1;";

		try {
			Cursor mCur = mDb.rawQuery(getAllUsers, null);

//			Log.i("Transaction", "CheckAvgCount: " + mCur.getCount());

			while (mCur.moveToNext()) {
				lastTransDate = mCur.getString(mCur.getColumnIndex("datetime"));
//				Log.i("Transaction", "LastDate: " + lastTransDate);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Date now;
		Date prev;
		long diffDays = 0;

		try {
			now = Format.parse(nowDateTime);
			prev = Format.parse(lastTransDate);

			long diff = now.getTime() - prev.getTime();
			diffDays = diff / (24 * 60 * 60 * 1000);
//			Log.i("Transaction", "DayDiff: " + diffDays);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (diffDays == 0) {
			/* app just lunched */
			avgQuan = 0;
		} else if (diffDays < 28) {
			/*
			 * formula will be [[ (sum of quantity sold of the product) / (date
			 * today - older transaction date) * 7 ]]
			 */
			try {
				String sqlGetTotalTransQuantity = "SELECT SUM(quantity) AS TotalQuantity "
						+ "FROM t_transactiondetail,t_user,t_transaction "
						+ "WHERE t_user.hubid="
						+ hubID
						+ " "
						+ "AND t_user.userid=t_transaction.userid "
						+ "AND t_transaction.transactionid=t_transactiondetail.transactionid "
						+ "AND t_transactiondetail.productid=" + productID;

				Cursor Cur = mDb.rawQuery(sqlGetTotalTransQuantity, null);
				while (Cur.moveToNext()) {
					float totalAmount = Cur.getFloat(Cur
							.getColumnIndex("TotalQuantity"));
					avgQuan = (totalAmount) / (diffDays * 7);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			/*
			 * formula will be [[ ((Sum of sold quantity in last 28 days) / 28)
			 * * 7 ]]
			 */
			try {
				String sqlGetTotalTransQuantity = "SELECT SUM(quantity) AS TotalQuantity "
						+ "FROM t_transactiondetail,t_user,t_transaction "
						+ "WHERE t_user.hubid="
						+ hubID
						+ " "
						+ "AND t_user.userid=t_transaction.userid "
						+ "AND t_transaction.transactionid=t_transactiondetail.transactionid "
						+ "AND t_transactiondetail.productid="
						+ productID
						+ " "
						+ "AND '"
						+ prevDateTime
						+ "' <= t_transaction.datetime";

				Cursor Cur = mDb.rawQuery(sqlGetTotalTransQuantity, null);
				while (Cur.moveToNext()) {
					float totalAmount = Cur.getFloat(Cur
							.getColumnIndex("TotalQuantity"));
					avgQuan = (totalAmount) / (28 * 7);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return avgQuan;

	}

	// function for deleting one product from transaction detail table

	public boolean deleteProduct(int transactionID) {
		boolean status;
		try {
			// searching for transaction details record
			String updateQuantity = "DELETE FROM t_transactiondetail WHERE transactiondid='"
					+ transactionID + "';";
			mDb.execSQL(updateQuantity);
			Log.i("Transaction", "DELETEQuary: " + updateQuantity);
			status = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		return status;
	}

	// function for getting customer information from database
	public ArrayList<UserContent> getCustomer(String QRCode) {
		UserContent user = new UserContent();
		ArrayList<UserContent> getCustomer = new ArrayList<UserContent>();

		try {
			String[] args = { QRCode };
			String getUserID = "SELECT * FROM t_qrcode where qrcode =?";
			Log.i("Transaction", "query: " + getUserID);

			Cursor cursor = mDb.rawQuery(getUserID, new String[] { QRCode });
			while (cursor.moveToNext()) {
				int user_id = cursor.getInt(cursor.getColumnIndex("userid"));

				System.err.println("user id = ==" + user_id);

				String query = "SELECT usertype FROM t_user where userid = '"
						+ user_id + "'";
				Cursor c = mDb.rawQuery(query, null);
				System.out.println();

				while (c.moveToNext()) {
					int type = c.getInt(c.getColumnIndex("usertype"));

					if (type == 1) {

						try {
							String getAparajita = "SELECT firstname, lastname, mobno FROM t_aparajita where userid = '"
									+ user_id + "'";
							Cursor aCur = mDb.rawQuery(getAparajita, null);
							while (aCur.moveToNext()) {
								user.userID = user_id;
								user.firstName = aCur.getString(aCur
										.getColumnIndex("firstname"));
								user.lastName = aCur.getString(aCur
										.getColumnIndex("lastname"));
								user.fullName = user.firstName + " "
										+ user.lastName;
								user.mobileNo = aCur.getString(aCur
										.getColumnIndex("mobno"));

								System.out.println("firstname:"
										+ user.firstName + " last: "
										+ user.lastName + " full: "
										+ user.fullName);
								/*
								 * Transaction customerInfo = new
								 * Transaction(mContext, userID, fullName,
								 * mobileNo); getCustomer.add(customerInfo);
								 */
								getCustomer.add(user);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (type == 5) {

						try {
							String getDist = "SELECT distname, edistname FROM t_distributor where userid = '"
									+ user_id + "'";
							Cursor dCur = mDb.rawQuery(getDist, null);

							while (dCur.moveToNext()) {
								user.fullName = dCur.getString(dCur
										.getColumnIndex("distname"));
								user.mobileNo = dCur.getString(dCur
										.getColumnIndex("edistname"));
								int userID = user_id;

								/*
								 * Transaction customerInfo = new
								 * Transaction(mContext, userID, fullName,
								 * mobileNo);
								 */
								getCustomer.add(user);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {

					}

				}
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return getCustomer;

	}

	public ArrayList<UserContent> getQRContent(CharSequence qrCode) {

		ArrayList<UserContent> userContent = new ArrayList<UserContent>();
		try {

			String getQRCode = "SELECT * FROM t_qrcode where qrcode = ?";

			Cursor cursor = mDb.rawQuery(getQRCode,
					new String[] { qrCode.toString() });
			while (cursor.moveToNext()) {
				int user_id = cursor.getInt(cursor.getColumnIndex("userid"));
				System.err.println("user id = ==" + user_id);
				String query = "SELECT * FROM t_user where userid = '"
						+ user_id + "'";
				Cursor c = mDb.rawQuery(query, null);
				System.out.println();

				while (c.moveToNext()) {
					int type = c.getInt(c.getColumnIndex("usertype"));
					UserContent user = new UserContent();

					user.userID = c.getInt(c.getColumnIndex("userid"));
					user.hubID = c.getInt(c.getColumnIndex("hubid"));
					user.type = c.getInt(c.getColumnIndex("usertype"));

					// userContent.add(user);

					if (type == 1) {

						try {
							String getAparajita = "SELECT firstname, lastname, mobno FROM t_aparajita where userid = '"
									+ user_id + "'";
							Cursor aCur = mDb.rawQuery(getAparajita, null);
							while (aCur.moveToNext()) {
								user.userID = user_id;
								user.firstName = aCur.getString(aCur
										.getColumnIndex("firstname"));
								user.lastName = aCur.getString(aCur
										.getColumnIndex("lastname"));
								user.fullName = user.firstName + " "
										+ user.lastName;
								user.mobileNo = aCur.getString(aCur
										.getColumnIndex("mobno"));

								System.out.println("firstname:"
										+ user.firstName + " last: "
										+ user.lastName + " full: "
										+ user.fullName);
								userContent.add(user);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else if (type == 5) {

						try {
							String getDist = "SELECT distname, edistname FROM t_distributor where userid = '"
									+ user_id + "'";
							Cursor dCur = mDb.rawQuery(getDist, null);

							while (dCur.moveToNext()) {
								user.fullName = dCur.getString(dCur
										.getColumnIndex("distname"));
								user.mobileNo = dCur.getString(dCur
										.getColumnIndex("edistname"));
								int userID = user_id;

								userContent.add(user);
							}
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					} else {
						System.out.println("user not in type 1 & 5");
					}
				}
			}
			cursor.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		return userContent;
	}

	public int[] toUpdateTransactionRow(int userID) {

		int[] transID = null;
		try {
			Cursor c = mDb.rawQuery(
					"SELECT * FROM t_transaction WHERE status = 'false' AND userid = '"
							+ userID + "'", null);
			transID = new int[c.getCount()];
			System.out.println("count:" + c.getCount());
			int i = 0;
			while (c.moveToNext()) {
				transID[i] = c.getInt(c.getColumnIndex("transactionid"));

				Log.e("Transaction", "TransID:" + transID[i]);
				i++;
			}
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return transID;
	}

	public ArrayList<HashMap<String, String>> getTransactionInfo(int userID) {

		ArrayList<HashMap<String, String>> transID = new ArrayList<HashMap<String, String>>();
		try {
			Cursor c = mDb.rawQuery(
					"SELECT * FROM t_transaction WHERE status = 'false' AND userid = '"
							+ userID + "'", null);
			// transID = new int[c.getCount()];
			System.out.println("count:" + c.getCount());
			while (c.moveToNext()) {
				HashMap<String, String> map = new HashMap<String, String>();

				map.put("transactionid",
						c.getString(c.getColumnIndex("transactionid")));
				map.put("userid", c.getString(c.getColumnIndex("userid")));
				map.put("counterpartid",
						c.getString(c.getColumnIndex("counterpartid")));
				map.put("datetime", c.getString(c.getColumnIndex("datetime")));
				map.put("transactiontype",
						c.getString(c.getColumnIndex("transactiontype")));
				map.put("totalvalue",
						c.getString(c.getColumnIndex("totalvalue")));
				map.put("lat", c.getString(c.getColumnIndex("lat")));
				map.put("lon", c.getString(c.getColumnIndex("lon")));
				map.put("locacc", c.getString(c.getColumnIndex("locacc")));

				transID.add(map);

				Log.e("Transaction", "TransID:" + transID.toString());
			}
			c.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return transID;
	}

	public ArrayList<HashMap<String, String>> getTransactionItemDetails(
			int transID) {
		ArrayList<HashMap<String, String>> syncedAllTransaction = new ArrayList<HashMap<String, String>>();

		try {
			// SELECT * FROM t_transactiondetail, t_product WHERE
			// t_transactiondetail.productid=t_product.productid AND
			// transactiondatetime=? ORDER BY transactionid ASC";

			String transactionedInfo = "SELECT * FROM t_transactiondetail WHERE transactionid = ?";
			Cursor mCur = mDb.rawQuery(transactionedInfo,
					new String[] { String.valueOf(transID) });
			// System.out.println("Total Product: " + mCur.getCount());
			while (mCur.moveToNext()) {
				int transactionId = mCur.getInt(mCur
						.getColumnIndex("transactionid"));
				int productId = mCur.getInt(mCur.getColumnIndex("productid"));
				int productQuantity = mCur.getInt(mCur
						.getColumnIndex("quantity"));
				float productPrice = mCur
						.getFloat(mCur.getColumnIndex("price"));
				float subtotalvalue = mCur.getFloat(mCur
						.getColumnIndex("subtotalvalue"));

				HashMap<String, String> map = new HashMap<String, String>();
				// System.out.println("=" +productId+ "=" +productQuantity+ "="
				// +productPrice+ "=" +subtotalvalue);
				map.put("transactionid", String.valueOf(transactionId));
				map.put("productId", String.valueOf(productId));
				map.put("productQuantity", String.valueOf(productQuantity));
				map.put("productPrice", String.valueOf(productPrice));
				map.put("subtotalvalue", String.valueOf(subtotalvalue));

				syncedAllTransaction.add(map);
			}
			mCur.close();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return syncedAllTransaction;
	}

	public void updateTransactionStatus(int[] transID) {
		// TODO Auto-generated method stub
		// int count = transID.length;
		System.out.println("length trans id: " + transID.length);
		// String insert =
		// "UPDATE t_transaction SET status = '1' WHERE transactionid IN (?,?) ";
		// mDb.execSQL(insert, new String[]{"1","2"});
		for (int i = 0; i < transID.length; i++) {
//			System.out.println("iii == " + i + "=" + transID[i]);
			String insert = "UPDATE t_transaction SET status = '1' WHERE transactionid IN (?) ";
			mDb.execSQL(insert, new String[] { "" + transID[i] });
		}
	}

	public String getUserQRCode(int userid) {

		String userQRCode = "";
		Cursor cursor = mDb.rawQuery(
				"SELECT qrcode FROM t_qrcode WHERE userid = " + userid + "",
				null);
		while (cursor.moveToNext()) {
			userQRCode = cursor.getString(cursor.getColumnIndex("qrcode"));
		}

		return userQRCode;
	}

	public ArrayList<HashMap<String, String>> getProductVersionInfo() {
		// TODO Auto-generated method stub

		ArrayList<HashMap<String, String>> productList = new ArrayList<HashMap<String, String>>();
		String query = "SELECT * FROM t_product";
		Cursor cursor = mDb.rawQuery(query, null);

		while (cursor.moveToNext()) {
			HashMap<String, String> map = new HashMap<String, String>();

			map.put("productid", String.valueOf(cursor.getInt(cursor
					.getColumnIndex("productid"))));
			map.put("productversion", String.valueOf(cursor.getInt(cursor
					.getColumnIndex("productversion"))));

			productList.add(map);

		}
		return productList;
	}

	public String findInAllUserQRCode(String userQR) {

		String userQRCode = "";
		String getQRCode = "SELECT * FROM t_qrcode where qrcode = ?";

		Cursor cursor = mDb.rawQuery(getQRCode, new String[] { userQR });
		while (cursor.moveToNext()) {
			userQRCode = cursor.getString(cursor.getColumnIndex("qrcode"));
		}
		System.out.println("qr ::::::::::::" + userQRCode);
		return userQRCode;
	}

	public boolean checkUserQRCode(String userQR) {

		String userQRCode = "";
		boolean status;
		try {
			String getQRCode = "SELECT * FROM t_user where qrcode = ?";

			Cursor cursor = mDb.rawQuery(getQRCode, new String[] { userQR });
			int countUser = cursor.getCount();

			if (countUser > 0) {
				status = true;
			} else {
				status = false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}

		return status;
	}

	public void updateProductInfo(
			ArrayList<HashMap<String, String>> arrayList_productInfo) {
		// TODO Auto-generated method stub
		if (arrayList_productInfo.size() > 0) {
			for (HashMap<String, String> hashmap : arrayList_productInfo) {
				System.out.println("trans file list of prod_version: "
						+ arrayList_productInfo.toString());

				String productid = hashmap.get("productid");
				String productname1 = hashmap.get("productname1");
				String productname2 = hashmap.get("productname2");
				String productversion = hashmap.get("productversion");
				String isNewProduct = hashmap.get("new");
				String productimage = hashmap.get("productimage");
				String eproductname1 = hashmap.get("eproductname1");
				String status = hashmap.get("status");
				String eproductname2 = hashmap.get("eproductname2");
				String tp = hashmap.get("tp");
				String mrp = hashmap.get("mrp");
				String createdby = hashmap.get("createdby");
				String specialoffer = hashmap.get("specialoffer");
				String categoryid = hashmap.get("categoryid");
				String unitid = hashmap.get("unitid");
				String updatedby = hashmap.get("updatedby");
				String updated = hashmap.get("updated");
				String created = hashmap.get("created");
				String pp = hashmap.get("pp");
				String manufid = hashmap.get("manufid");
				System.out.println("" + productid + "\\" + productversion);

				ContentValues values = new ContentValues();

				values.put("productname1", productname1);
				values.put("productname2", productname2);
				values.put("productversion", productversion);
				values.put("productimage", productimage);
				values.put("eproductname1", eproductname1);
				values.put("status", status);
				values.put("eproductname2", eproductname2);
				values.put("tp", tp);
				values.put("mrp", mrp);
				values.put("createdby", createdby);
				values.put("specialoffer", specialoffer);
				values.put("categoryid", categoryid);
				values.put("unitid", unitid);
				values.put("updatedby", updatedby);
				values.put("updated", updated);
				values.put("created", created);
				values.put("pp", pp);
				values.put("manufid", manufid);

				if (isNewProduct.equals("false")) {
					/*
					 * String queryUpdateProduct =
					 * "UPDATE t_product SET (productname1,productversion) VALUES "
					 * +
					 * "("+productname1+","+productversion+") WHERE productid = ?"
					 * ;
					 * 
					 * mDb.execSQL(queryUpdateProduct, new String[]{productid});
					 */

					mDb.update("t_product", values, "productid = ?",
							new String[] { productid });

				} else if (isNewProduct.equals("true")) {
					/*
					 * String queryInsertProduct =
					 * "INSERT t_product SET (productversion) values " +
					 * "('+productversion+') WHERE productid = ?";
					 */
					values.put("productid", productid);
					mDb.insert("t_product", null, values);

					DownloadAndSave downSave = new DownloadAndSave(mContext);
					downSave.getBitmapFromURL(productimage);

				}

			}
		} else {
			System.out.println("no data" + arrayList_productInfo);
		}

	}

	public List<SalesBy> getSalesByPerson(int hubId) {
		// TODO Auto-generated method stub
		List<SalesBy> salesToPersonLists = new ArrayList<SalesBy>();
		String salesTo_query = "SELECT usertype,userid FROM t_user WHERE hubid = ?";
		Cursor cursor = mDb.rawQuery(salesTo_query,
				new String[] { String.valueOf(hubId) });

		while (cursor.moveToNext()) {

			int userType = cursor.getInt(cursor.getColumnIndex("usertype"));
			int userId = cursor.getInt(cursor.getColumnIndex("userid"));
			if (userType == 2) {
				String query = "SELECT * FROM t_serviceperson WHERE hubid = '"
						+ hubId + "' AND userid = '" + userId + "'";
				Cursor c = mDb.rawQuery(query, null);
				while (c.moveToNext()) {
					Sales sales = new Sales();
					SalesBy salesBy = sales.new SalesBy();
					// salesTo.personLogo = c.getString(c.getColumnIndex(""));
					salesBy.personId = c.getInt(c.getColumnIndex("userid"));
					salesBy.personFirstName = c.getString(c
							.getColumnIndex("firstname"));
					salesBy.personLastName = c.getString(c
							.getColumnIndex("lastname"));
					salesBy.personName = "" + salesBy.personFirstName + " "
							+ salesBy.personLastName;
					System.out.println("service name :" + salesBy.personName);
					salesToPersonLists.add(salesBy);
				}

			}/*
			 * else if(userType == 1){ String query =
			 * "SELECT * FROM t_aparajita WHERE hubid = '"
			 * +hubId+"' AND userid = '"+userId+"'"; Cursor c =
			 * mDb.rawQuery(query, null); while (c.moveToNext()) { SalesTo
			 * salesTo = new SalesTo(); // salesTo.personLogo =
			 * c.getString(c.getColumnIndex("")); salesTo.personFirstName =
			 * c.getString(c.getColumnIndex("firstname"));
			 * salesTo.personLastName =
			 * c.getString(c.getColumnIndex("lastname")); salesTo.personName =
			 * salesTo.personFirstName + salesTo.personLastName;
			 * salesToPersonLists.add(salesTo); }
			 * 
			 * }
			 */
		}
		return salesToPersonLists;
	}

	public List<SalesTo> getSalesToPerson(int hubId) {
		// TODO Auto-generated method stub
		List<SalesTo> salesToPersonLists = new ArrayList<SalesTo>();
		String salesTo_query = "SELECT usertype,userid FROM t_user WHERE hubid = ?";
		Cursor cursor = mDb.rawQuery(salesTo_query, new String[]{String.valueOf(hubId)});
		
		while (cursor.moveToNext()) {

			int userType = cursor.getInt(cursor.getColumnIndex("usertype"));
			int userId = cursor.getInt(cursor.getColumnIndex("userid")); 
			
			if(userType == 1){
				String query = "SELECT *,DATE(created) FROM t_aparajita WHERE hubid = '"+hubId+"' AND userid = '"+userId+"'";
				Cursor c = mDb.rawQuery(query, null);
				while (c.moveToNext()) {
					Sales sales = new Sales();
					SalesTo salesTo = sales.new SalesTo();
//					salesTo.personLogo = c.getString(c.getColumnIndex(""));
					salesTo.personId = c.getInt(c.getColumnIndex("userid"));
					salesTo.personFirstName = c.getString(c.getColumnIndex("firstname"));
					salesTo.personLastName = c.getString(c.getColumnIndex("lastname"));
					salesTo.personName = ""+salesTo.personFirstName +" "+ salesTo.personLastName;
					
//					salesTo.aparajitaVillage = c.getString(c.getColumnIndex(""));
					float lat = c.getFloat(c.getColumnIndex("lat"));
					float lon = c.getFloat(c.getColumnIndex("lon"));
					String location = ""+ lat+", "+""+lon;
					salesTo.aparajitaLocation = location;
//					salesTo.aparajitaDistance = c.getString(c.getColumnIndex(""));
					salesTo.aparajitaJoined = c.getString(c.getColumnIndex("created"));
					/*SimpleDateFormat mDateFormat =new SimpleDateFormat("yyyy-MM-dd ");
					try {
						salesTo.aparajitaJoined = String.valueOf(mDateFormat.parse(c.getString(c.getColumnIndex("created"))));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}*/
					System.out.println("aparajita name :" + salesTo.personName);
					salesToPersonLists.add(salesTo);
				}
				
			}
		}
		return salesToPersonLists;
	}

	public List<YesterdaySalesInfo> getSalesInfo(int personID) {
		// TODO Auto-generated method stub
		List<YesterdaySalesInfo> yesterdaySalesInfoList = new ArrayList<Sales.YesterdaySalesInfo>();

		String query_salesInfo = "SELECT transactionid FROM t_transaction WHERE userid = ? ";

		Cursor salesInfo_cursor = mDb.rawQuery(query_salesInfo,
				new String[] { String.valueOf(personID) });
		while (salesInfo_cursor.moveToNext()) {
			int transactionid = salesInfo_cursor.getInt(salesInfo_cursor
					.getColumnIndex("transactionid"));

			String query_salesDetails = "SELECT * FROM t_transactiondetail,t_product WHERE t_transactiondetail.productid = t_product.productid AND transactionid = '"
					+ transactionid + "'";
			Cursor cursor_salesDetails = mDb.rawQuery(query_salesDetails, null);
			while (cursor_salesDetails.moveToNext()) {
				Sales mSales = new Sales();
				YesterdaySalesInfo mYesterdaySalesInfo = mSales.new YesterdaySalesInfo();

				mYesterdaySalesInfo.pName = cursor_salesDetails
						.getString(cursor_salesDetails
								.getColumnIndex("eproductname1"));
				mYesterdaySalesInfo.pDesc = cursor_salesDetails
						.getString(cursor_salesDetails
								.getColumnIndex("eproductname2"));
				int pTotalQuantity = 0;
				pTotalQuantity = pTotalQuantity
						+ cursor_salesDetails.getInt(cursor_salesDetails
								.getColumnIndex("quantity"));

				mYesterdaySalesInfo.pTotalQuantity = pTotalQuantity;
				float pTotalPrice = 0;
				pTotalPrice = cursor_salesDetails.getFloat(cursor_salesDetails
						.getColumnIndex("subtotalvalue"));
				mYesterdaySalesInfo.pTotalPrice = pTotalPrice;
				mYesterdaySalesInfo.pp = cursor_salesDetails
						.getInt(cursor_salesDetails.getColumnIndex("pp"));
				mYesterdaySalesInfo.tp = cursor_salesDetails
						.getInt(cursor_salesDetails.getColumnIndex("tp"));
				mYesterdaySalesInfo.mrp = cursor_salesDetails
						.getInt(cursor_salesDetails.getColumnIndex("mrp"));

				yesterdaySalesInfoList.add(mYesterdaySalesInfo);
			}
		}
		return yesterdaySalesInfoList;
	}

	/*
	 * get transaction info 
	 */
	public List<TransactionsDetail> getTransactionId(int userId, String currentDate) {
		// TODO Auto-generated method stub
		List<TransactionsDetail> mList = new ArrayList<Sales.TransactionsDetail>();
		System.out.println("" +currentDate);
		String query_getTransInfo = "SELECT * FROM t_transaction t,t_transactiontype tt WHERE DATE(t.datetime) < DATE('"+currentDate+"') AND t.transactiontype = tt.transactiontype AND t.userid = '"+userId+"'";
		Cursor mCursor_tID = mDb.rawQuery(query_getTransInfo, null);

		while (mCursor_tID.moveToNext()) {

			int t_id = mCursor_tID.getInt(mCursor_tID.getColumnIndex("transactionid"));
			Sales mSales = new Sales();

			TransactionsDetail mTransactionsDetail = mSales.new TransactionsDetail();
			mTransactionsDetail.transactionId = t_id;
			mTransactionsDetail.transactionTypeDescrp = mCursor_tID.getString(mCursor_tID.getColumnIndex("description"));
			String datetime = mCursor_tID.getString(mCursor_tID.getColumnIndex("datetime"));
			mTransactionsDetail.transactionDate = datetime ;
			System.out.println("trans dte: "+mTransactionsDetail.transactionDate);
			
			mTransactionsDetail.transactionedValue = mCursor_tID.getFloat(mCursor_tID.getColumnIndex("totalvalue"));
			int transactionedToUser = mCursor_tID.getInt(mCursor_tID.getColumnIndex("counterpartid")) ;
			if(transactionedToUser == 1){
				mTransactionsDetail.transactionedToUser = "Others";
			}else {
				String query_getUserInfo = "SELECT * FROm t_user u,t_usertype ut WHERE u.usertype = ut.usertype AND u.userid = '"+transactionedToUser+"'";
				Cursor cursor_userInfo = mDb.rawQuery(query_getUserInfo, null);
				while (cursor_userInfo.moveToNext()) {
					int transUserId = cursor_userInfo.getInt(cursor_userInfo.getColumnIndex("userid"));
					int transUserType = cursor_userInfo.getInt(cursor_userInfo.getColumnIndex("usertype"));
					
					System.out.println("type :" +transUserType +" id :" +transUserId);
					
					if(transUserType == 1){
						String query = "SELECT * FROM t_aparajita WHERE userid = '"+transUserId+"'";
						Cursor c = mDb.rawQuery(query, null);
						while (c.moveToNext()) {
							
							String personFirstName = c.getString(c.getColumnIndex("firstname"));
							String personLastName = c.getString(c.getColumnIndex("lastname"));
							mTransactionsDetail.transactionedToUser = ""+personFirstName +" "+ personLastName;
							}
					}else if(transUserType == 5){
						
						String query = "SELECT * FROM t_distributor WHERE userid = '"+transUserId+"'";
						Cursor c = mDb.rawQuery(query, null);
						while (c.moveToNext()) {
							
							String personFirstName = c.getString(c.getColumnIndex("distname"));
							String personLastName = c.getString(c.getColumnIndex("edistname"));
							mTransactionsDetail.transactionedToUser = ""+personFirstName +" "+ personLastName;
						}
					}else if (transUserId == 2) {
						String query = "SELECT * FROM t_serviceperson WHERE userid = '" + transUserId + "'";
						Cursor c = mDb.rawQuery(query, null);
						while (c.moveToNext()) {
							
							String personFirstName = c.getString(c
									.getColumnIndex("firstname"));
							String personLastName = c.getString(c
									.getColumnIndex("lastname"));
							mTransactionsDetail.transactionedToUser = "" + personFirstName + " "
									+ personLastName;
							
						}
					}
				}
			}
//			Cursor conterpartId_cursor = mDb.rawQuery("Select", null);
			mList.add(mTransactionsDetail);

		}
		return mList;
	}

	public List<TransactionListItemDetail> getTransactionListItemDetails(int transID){

		ArrayList<TransactionListItemDetail> transactionListItem = new ArrayList<TransactionListItemDetail>();

		try {
			 /*SELECT * FROM t_transactiondetail, t_product WHERE
			 t_transactiondetail.productid=t_product.productid AND
			 transactiondatetime=? ORDER BY transactionid ASC*/

			String transactionedInfo = "SELECT * FROM t_transactiondetail, t_product WHERE t_transactiondetail.productid=t_product.productid AND transactionid = ? ORDER BY transactionid ASC";
			Cursor mCur = mDb.rawQuery(transactionedInfo,new String[] { String.valueOf(transID) });
			// System.out.println("Total Product: " + mCur.getCount());
			while (mCur.moveToNext()) {
				Sales mSales = new Sales();
				TransactionListItemDetail mTransactionListItemDetail = mSales.new TransactionListItemDetail();
				
				mTransactionListItemDetail.productName = mCur.getString(mCur.getColumnIndex("eproductname1"));
				mTransactionListItemDetail.productDesc = mCur.getString(mCur.getColumnIndex("eproductname2"));
				mTransactionListItemDetail.productQuantity = mCur.getInt(mCur.getColumnIndex("quantity"));
				mTransactionListItemDetail.productImg = mCur.getString(mCur.getColumnIndex("productimage"));
				mTransactionListItemDetail.productSubTotalValue = mCur.getFloat(mCur.getColumnIndex("subtotalvalue"));
				mTransactionListItemDetail.partnerPrice = mCur.getInt(mCur.getColumnIndex("pp"));
				mTransactionListItemDetail.tradePrice = mCur.getInt(mCur.getColumnIndex("tp"));
				mTransactionListItemDetail.retailPrice = mCur.getInt(mCur.getColumnIndex("mrp"));
				
				int pTotalAmount = 0;
				pTotalAmount = pTotalAmount+ mCur.getInt(mCur.getColumnIndex("subtotalvalue"));
				mTransactionListItemDetail.totalAmount = pTotalAmount;
				System.out.println(""+ pTotalAmount);
				transactionListItem.add(mTransactionListItemDetail);
			}
			mCur.close();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		return transactionListItem;
	
	}
	
	
	
	
	
	
	//function to delete last transaction product
	public boolean clearLastProduct(String dateTime, int transType) {
		
		boolean status = false;
		
		try {
			String clearLast = "DELETE FROM t_transactiondetail WHERE transactiondid=(SELECT transactiondid FROM t_transactiondetail WHERE transactiondatetime='" + dateTime + "' AND transactiontype="+ transType +" ORDER BY updated DESC LIMIT 1)";
			mDb.execSQL(clearLast);
			status = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			status = false;
		}
		
		return status;
	}
	
	
	
	//function to delete all transaction product
		public boolean clearAllProduct(String dateTime, int transType) {
			
			boolean status = false;
			
			try {
				String clearLast = "DELETE FROM t_transactiondetail WHERE transactiondatetime='" + dateTime + "' AND transactiontype="+ transType +"";
				mDb.execSQL(clearLast);
				status = true;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				status = false;
			}
			
			return status;
		}
}