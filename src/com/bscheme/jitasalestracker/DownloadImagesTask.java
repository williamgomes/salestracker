package com.bscheme.jitasalestracker;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class DownloadImagesTask extends AsyncTask<ImageView, Void, Drawable> {

	ImageView imageView = null;
	Context context;
	@Override
	protected Drawable doInBackground(ImageView... imageViews) {
		this.imageView = imageViews[0];
		return download_Image((String) imageView.getTag());
	}

	@Override
	protected void onPostExecute(Drawable result) {
		imageView.setImageDrawable(result);
	}

	private Drawable download_Image(String address) {
		try {
			Log.i("product details", "starting image download");
			URL url = new URL(address);
			URLConnection conn = url.openConnection();
			conn.connect();
			InputStream is = conn.getInputStream();
			Drawable d = Drawable.createFromStream(is, "src name");
			is.close();
			return d;
		} catch (Exception e) {
			Log.e("the url", address + ", error: " + e);
			e.printStackTrace();
			
			return context.getResources().getDrawable(R.drawable.noimage);
		}
	}
}
