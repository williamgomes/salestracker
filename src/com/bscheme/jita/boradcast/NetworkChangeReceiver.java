package com.bscheme.jita.boradcast;

import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONException;
import org.json.JSONObject;

import com.bscheme.jitasalestracker.SalesActivity;
import com.bscheme.sync.SyncProductVersionToUpdate;
import com.bscheme.sync.SyncTransaction;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.Toast;

public class NetworkChangeReceiver extends BroadcastReceiver {
	public Context mContext;
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpref;
	SharedPreferences.Editor editor;
	public static final String HubID = "HubIDKey"; 
	public static final String UserKeyID = "UserIDKey"; 
	int userID;
	int hubID;
	// JSON Node names
	private static final String TAG_SUCCESS = "success";

	@Override
	public void onReceive(final Context context, final Intent intent) {
		this.mContext = context;
		
		sharedpref = mContext.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
		editor = sharedpref.edit();
		
		hubID = sharedpref.getInt(HubID, 0);
		userID = sharedpref.getInt(UserKeyID, 0);
		System.out.println("userid from broadcast:  " +userID);
		String status = NetworkUtil.getConnectivityStatusString(context);

		Toast.makeText(context, "jita sales Tracker: " + status,	Toast.LENGTH_SHORT).show();
		
		boolean conn = NetworkUtil.isInternetOn(mContext);
		if (conn) {
//			SyncTransaction sync = new SyncTransaction(mContext);
//			sync.postJSON(String.valueOf(userID));
			Bundle bandle = new Bundle();		
			Intent serciveIntent = new Intent(mContext, SyncProductVersionToUpdate.class);
			bandle.putInt("timerValue", 2*60*60*1000);
			serciveIntent.putExtras(bandle);
			
			mContext.startService(serciveIntent);
//			mContext.startService(new Intent(mContext, SyncProductVersionToUpdate.class));
		}
		
		if(conn){
			Bundle bandle = new Bundle();		
			Intent serciveIntent = new Intent(mContext, SyncTransaction.class);
			bandle.putInt("timerValue", 2*60*60*1000);
			serciveIntent.putExtras(bandle);
			
			mContext.startService(serciveIntent);
			/*SyncProductVersionToUpdate syncProduct = new SyncProductVersionToUpdate(mContext);
			syncProduct.postJSON(hubID,userID);*/
		}
		
	}

}
