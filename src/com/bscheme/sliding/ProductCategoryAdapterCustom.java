package com.bscheme.sliding;

import java.util.ArrayList;
import com.bscheme.jitasalestracker.DownloadImagesTask;
import com.bscheme.jitasalestracker.Product;
import com.bscheme.jitasalestracker.R;
import com.bscheme.util.ImageDownloaderTask;
import com.bscheme.util.Urls;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ProductCategoryAdapterCustom extends BaseAdapter {

	String productName;
	String productDesc;
	String productImg;
	int productID;
	int productCatID;
	float partnerPrice;
	float tradePrice;
	float retailPrice;
	float productQuantity;
	public static final String MyPREFERENCES = "MyPrefs" ;
	public static final String TIME = "TimeKey"; 
	SharedPreferences sharedpreferences;
	
	ArrayList<Product> productList = new ArrayList<Product>();
	Context mContext;
	private int hubID;

	int productCategoryID;

	ProductCategoryAdapterCustom(Context context, int productCategoryId, int hubId) {

		this.mContext = context;
		this.hubID = hubId;
		this.productCategoryID = productCategoryId;
		Product product = new Product(mContext);
		product.open();

		productList = product.getProductsInSales(mContext,productCategoryID, hubID);

		product.close();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return productList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return productList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	class ViewHolder {

		TextView productTitle, productDescription,productIDv,productPrice;
		ImageView productImg;

		ViewHolder(View view) {
			productTitle = (TextView) view.findViewById(R.id.textTitle);
			productDescription = (TextView) view.findViewById(R.id.textDesc);
			productImg = (ImageView) view.findViewById(R.id.imgProduct);
			productIDv = (TextView) view.findViewById(R.id.textID);
			productPrice = (TextView) view.findViewById(R.id.textPrice);

		}
	}

	@Override
	public View getView(int position, View view, ViewGroup productViewGroup) {
		// TODO Auto-generated method stub
		View productView = view;
		ViewHolder vholder = null;


		if (productView == null) { // if it's not recycled, initialize some
									// attributes
			LayoutInflater inflater = (LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			productView = inflater.inflate(R.layout.single_product,
					productViewGroup, false);
			vholder = new ViewHolder(productView);
			productView.setTag(vholder);
		} else {
			vholder = (ViewHolder) productView.getTag();
		}

		Product tempProduct = productList.get(position);
		vholder.productTitle.setText(tempProduct.productName);
		vholder.productDescription.setText(tempProduct.productDesc);

		String productImageURL = tempProduct.productImg;
		Urls urls = new Urls();
		//System.out.println(""+productImageURL);
		String URL = urls.downloadImageUrl + "android.png";
//		String URL = "http://95.85.22.158/upload/product/small/446_572.jpg";
		
		String productPrice = "" + tempProduct.tradePrice + " | " + tempProduct.retailPrice;
		
		vholder.productPrice.setText(productPrice);
//		vholder.productImg.setTag(URL);
		vholder.productIDv.setTag(tempProduct);
		DownloadImagesTask dlit = new DownloadImagesTask();
//		dlit.execute(vholder.productImg);
		
		
		ImageDownloaderTask imgDownload = new ImageDownloaderTask(mContext,vholder.productImg,URL);
		imgDownload.execute();
		
		return productView;
	}
	
	public void onBackPressed()
	{
		
	   // do nothing to disable back button
	} 

}