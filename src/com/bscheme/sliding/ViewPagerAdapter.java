package com.bscheme.sliding;

import java.util.ArrayList;
import java.util.List;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ViewPagerAdapter extends PagerAdapter {

	List<View> viewList = new ArrayList<View>();
	public ViewPagerAdapter(List<View> viewLists) {
		// TODO Auto-generated constructor stub
		viewList = viewLists;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return viewList.size();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		// TODO Auto-generated method stub
		return arg0 == arg1;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		// TODO Auto-generated method stub
		container.removeView(viewList.get(position));
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		// TODO Auto-generated method stub
		container.addView(viewList.get(position),0);
		
		/*switch (position) {
		case 1:
			tv_ProductCategory.setText("Category 1");
			break;
		case 2:
			tv_ProductCategory.setText("Category 2");
			break;
		case 3:
			tv_ProductCategory.setText("Category 3");
			break;
			

		default:
			break;
		}*/
		return viewList.get(position);
	}

	/* (non-Javadoc)
	 * @see android.support.v4.view.PagerAdapter#notifyDataSetChanged()
	 */
	@Override
	public void notifyDataSetChanged() {
		// TODO Auto-generated method stub
		super.notifyDataSetChanged();
		System.out.println("changed view");
	}

}
