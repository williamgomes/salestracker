package com.bscheme.util;

public class Sales {

	public class SalesTo {
		public String personLogo;
		public String personFirstName;
		public String personLastName;
		public String personName;
		public int personId;
		public String aparajitaLocation;
		public String aparajitaVillage;
		public String aparajitaDistance;
		public String aparajitaJoined;
	}
	
	public class SalesBy {
		public String personLogo;
		public String personFirstName;
		public String personLastName;
		public String personName;
		public int personId;
	}
	public class YesterdaySalesInfo{
		public String pName;
		public String pDesc;
		public int pTotalQuantity;
		public float pTotalPrice;
		public int pp;
		public int tp;
		public int mrp;
	}
	public class AparajitaInfo{
		public String aparajitaLogo;
		public String aparajitaFirstName;
		public String aparajitaLastName;
		public String aparajitaName;
		public String aparajitaLocation;
		public String aparajitaVillage;
		public String aparajitaDistance;
		public String aparajitaJoined;
		
	}
	
	public class TransactionsDetail{
		public int transactionId;
		public String transactionTypeDescrp;
		public String transactionDate;
		public String transactionedToUser;
		public float transactionedValue;
	}
	/**
	 * class for 
	 * product details for transaction lists 
	 * @author Newton
	 *
	 */
	public class TransactionListItemDetail{
		public String productName;
		public String productDesc;
		public String productImg;
		public int productCatID;
		public int productID;
		public float tradePrice;
		public float partnerPrice;
		public float retailPrice;
		public float productQuantity;
		public float productSubTotalValue;
		public float totalAmount;
	}
}
