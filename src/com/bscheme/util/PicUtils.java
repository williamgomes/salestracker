package com.bscheme.util;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.os.Environment;

public class PicUtils {
	  public File getSavePath(String folderName) {
	      File path, subFolder;
	      if (hasSDCard()) { // SD card
	          path = new File(getSDCardPath() + "/Jita/"+folderName);
	          if(!path.exists()){
	        	  path.mkdirs();
	        	  subFolder = new File(path+folderName);
	        	  if(!subFolder.exists()){
//	        		  subFolder.mkdirs();
	        	  }
	          }else{
	        	  subFolder = new File(path+folderName);
	        	  if(!subFolder.exists()){
//	        		  subFolder.mkdirs();
	        	  }
	          }
	         
	      } else { 
	          path = Environment.getDataDirectory();
	      }
	      return path;
	  }
	  public String getCacheFilename(String folderName, String imageName) {
	      File f = getSavePath(folderName);
	      return f.getAbsolutePath() + "/"+ imageName;
	  }

	  public Bitmap loadFromFile(String filename) {
	      try {
	          File f = new File(filename);
	          if (!f.exists()) { return null; }
	          Bitmap tmp = BitmapFactory.decodeFile(filename);
	          return tmp;
	      } catch (Exception e) {
	          return null;
	      }
	  }
	  public Bitmap loadFromCacheFile(String folderName,String imageName) {
	      return loadFromFile(getCacheFilename(folderName,imageName));
	  }
	  public void saveToCacheFile(String folderName,String imageName,Bitmap bmp) {
	      saveToFile(getCacheFilename(folderName,imageName),bmp);
	  }
	  public void saveToFile(String filename,Bitmap bmp) {
	      try {
	          FileOutputStream out = new FileOutputStream(filename);
	          bmp.compress(CompressFormat.PNG, 100, out);
	          out.flush();
	          out.close();
	      } catch(Exception e) {}
	  }

	  public boolean hasSDCard() { // SD????????
	      String status = Environment.getExternalStorageState();
	      return status.equals(Environment.MEDIA_MOUNTED);
	  }
	  public String getSDCardPath() {
	      File path = Environment.getExternalStorageDirectory();
	      return path.getAbsolutePath();
	  }

	}