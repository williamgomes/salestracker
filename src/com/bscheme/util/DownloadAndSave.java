package com.bscheme.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

public class DownloadAndSave {

	
	public Bitmap bmp;
	String fileName;
	String fileNameWithoutExtn;
	Context c;
	public DownloadAndSave(Context context){
		this.c = context;
	}
	public Bitmap getBitmapFromURL(String link) {
		/*--- this method downloads an Image from the given URL, 
		 *  then decodes and returns a Bitmap object
		 ---*/
		
		ConnectionDetector internet = new ConnectionDetector(c);
		boolean connectivity = internet.isConnectingToInternet();
		if(connectivity){
			try {
				URL url = new URL(link);
				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setDoInput(true);
				connection.connect();
				InputStream input = connection.getInputStream();
				Bitmap myBitmap = BitmapFactory.decodeStream(input);

				fileName = link.substring( link.lastIndexOf('/')+1, link.length() );
				fileNameWithoutExtn = fileName.substring(0, fileName.lastIndexOf('.'));
				
				bmp = myBitmap;
				saveImageToSD(fileName);
				return myBitmap;

			} catch (IOException e) {
				e.printStackTrace();
				Log.e("getBmpFromUrl error: ", e.getMessage().toString());
				return null;
			}
		}
		return null;
	}
	
	public File saveImageToSD(String imageName) {

		/*--- this method will save your downloaded image to SD card ---*/

		FileOutputStream fos = null;
		
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		/*--- you can select your preferred CompressFormat and quality. 
		 * I'm going to use JPEG and 100% quality ---*/
		bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
		/*--- create a new file on SD card ---*/
	
		File folder = new File(Environment.getExternalStorageDirectory(), "/productLogo/");
		if(! folder.exists()){
			folder.mkdir();
//			System.out.println("folder not found : " +folder.toString());
		}else {
//			System.out.println("folder found : " +folder.toString());
		}
		File file = new File(folder.getAbsolutePath(), imageName);
		try {
			file.createNewFile();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*--- create a new FileOutputStream and write bytes to file ---*/
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			fos.write(bytes.toByteArray());
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return file;
	}
}
