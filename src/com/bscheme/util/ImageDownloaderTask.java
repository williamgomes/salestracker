package com.bscheme.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.bscheme.jitasalestracker.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;
public class ImageDownloaderTask extends AsyncTask<String, Void, Bitmap>{

	ImageView img;
	String url;
	public Context c;
	
	Bitmap bmp;
	String imageFileName;
	String fileNameWithoutExtn;
	File imageFilePath;
	// 
	public ImageDownloaderTask (Context c,ImageView img, String url){
		this.img = img;
		this.url = url;
		this.c = c;
	}
	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		System.out.println();
		super.onPreExecute();
	}
	@Override
	protected Bitmap doInBackground(String... arg0) {
		// TODO Auto-generated method stub
		imageFileName = url.substring( url.lastIndexOf('/')+1, url.length() );
		fileNameWithoutExtn = imageFileName.substring(0, imageFileName.lastIndexOf('.'));
		ConnectionDetector internet = new ConnectionDetector(c);
		boolean connectivity = internet.isConnectingToInternet();
		PicUtils mPicUtils = new PicUtils();
		Bitmap bitmapFromLocal = mPicUtils.loadFromCacheFile("product_image", imageFileName);
		if(connectivity){
			if(bitmapFromLocal != null){
				return bitmapFromLocal;
			}else{
				bmp = getBitmapFromURL(url);
				
				mPicUtils.saveToCacheFile("product_image", imageFileName, bmp);
//				imageFilePath = saveImageToSD(bmp,fileName);
			}
		}
		
		return bitmapFromLocal;
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		/*if( ! imageFilePath.equals(fileName)){
//			img.setImageBitmap(imageFilePath.getAbsoluteFile())
		}*/
		if(result != null){
			img.setImageBitmap(result);
		}else{
			img.setImageResource(R.drawable.noimage);
		}
		
	}

	public Bitmap getBitmapFromURL(String link) {
		/*--- this method downloads an Image from the given URL, 
		 *  then decodes and returns a Bitmap object
		 ---*/
		ConnectionDetector internet = new ConnectionDetector(c);
		boolean connectivity = internet.isConnectingToInternet();
		if(connectivity){
			try {
				URL url = new URL(link);
				HttpURLConnection connection = (HttpURLConnection) url
						.openConnection();
				connection.setDoInput(true);
				connection.connect();
				InputStream input = connection.getInputStream();
				Bitmap myBitmap = BitmapFactory.decodeStream(input);

				return myBitmap;

			} catch (IOException e) {
				e.printStackTrace();
				Log.e("getBmpFromUrl error: ", e.getMessage().toString());
				return null;
			}
		}
		else{
			Toast.makeText(c, "No Internet", Toast.LENGTH_SHORT).show();
		}
		return null;
	}
	
	public File saveImageToSD(Bitmap bitmap,String imageName) {

		/*--- this method will save your downloaded image to SD card ---*/

		FileOutputStream fos = null;
		
		
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		/*--- you can select your preferred CompressFormat and quality. 
		 * I'm going to use JPEG and 100% quality ---*/
		if(bitmap != null){
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
			/*--- create a new file on SD card ---*/
		
			File folder = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "/Jita/");
			if(! folder.exists()){
				folder.mkdirs();
				System.out.println("folder not found : " +folder.toString());
			}else {
				System.out.println("folder found : " +folder.toString());
			}
			File file = new File(folder, imageName);
			try {
				file.createNewFile();
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			/*--- create a new FileOutputStream and write bytes to file ---*/
			try {
				fos = new FileOutputStream(file.getAbsolutePath());
//				bitmap.compress(CompressFormat.JPEG, 80, fos);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			try {
				fos.write(bytes.toByteArray());
				fos.flush();
				fos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return file;
		}else{
//			Toast.makeText(c, "No Image or Invalid", Toast.LENGTH_SHORT).show();
		}
		return null;
	}
	
}
