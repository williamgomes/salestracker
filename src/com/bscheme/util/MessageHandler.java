package com.bscheme.util;

import com.bscheme.jitasalestracker.Message;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

public class MessageHandler extends Activity{

	private Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
//		super.onCreate(savedInstanceState);
		
		handler = new Handler(){
			public void handleMessage(android.os.Message msg){
				Toast.makeText(MessageHandler.this, "test message", Toast.LENGTH_SHORT).show();
			}
		};
	}
	
	public Handler returnHandler(){
        return handler;
    }

}
